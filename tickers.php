<?php

function getTicker($exchange, $pair, $counter){
    $dir_ticker = '/home/talisant/tickers/' . $exchange . '/';
    $in_ticker_folder = scandir($dir_ticker . $pair);
    if (in_array("ticker.json", $in_ticker_folder)) {
        $str = file_get_contents($dir_ticker . $pair . '/ticker.json');
        if($str == '' && $counter < 20){
            sleep(1);
            getTicker($exchange, $pair, $counter);
        }else{
            return $str;
        }
    }
}

if(isset($_POST['exchange'])) {

    $dir_ticker = '/home/talisant/tickers/'.$_POST['exchange'].'/';
    $ticker_folders = scandir($dir_ticker);

    array_shift($ticker_folders);
    array_shift($ticker_folders);

    foreach ($ticker_folders as $ticker_folder) {

        $in_ticker_folder = scandir($dir_ticker . $ticker_folder);
//        print_r($dir_ticker . $ticker_folder);
        if (in_array("ticker.json", $in_ticker_folder)) {
            $tickers[] = $ticker_folder;
        }
    }


    foreach($tickers as $ticker){
        echo "<option value='$ticker'>$ticker</option>/n";
    }


}elseif (isset($_POST['pair'])){
    $exchange = $_POST['exchange_'];
    $pair = $_POST['pair'];
    $counter = 1;
    $str = getTicker($exchange, $pair, $counter);

//    $dir_ticker = '/home/talisant/tickers/'.$_POST['exchange_'].'/';
//    $in_ticker_folder = scandir($dir_ticker . $_POST['pair']);
//    if (in_array("ticker.json", $in_ticker_folder)) {
//        $str = file_get_contents($dir_ticker . $_POST['pair'].'/ticker.json');
//    }

    $json = json_decode($str, true);
    $json['time'] = mb_substr($json['time'], 0, -3);

    $ask = $json['ask'];
    $bid = $json['bid'];
    $time = $json['time'];
    $date = date('m-d H:i:s', $time+(4*3600));
    echo "<div class='tickers'><span>ask:</span> $ask<span> bid:</span> $bid <span>Date:</span> $date</div>";
}
