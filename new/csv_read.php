<?php
//print_r($_POST);
//exit;
//ini_set("display_errors", 1);
//error_reporting(E_ALL);
if (isset($_POST['id'])) {
    $id = $_POST['id'];
    $file_name = $_POST['file'];
    $file_ = "$file_name.csv";
    if ($file_name == 'newOrderInfo') {
        $file_ = "$file_name";
    }
    $dir = $_SERVER['DOCUMENT_ROOT'] . "/new/bot/$id";
    $array_in = scandir("$dir");

    if (!in_array($file_, $array_in)) {
        echo "$file_ not found";
        die;
    }
    if ($file_name == 'newOrderInfo') {
        $content = file_get_contents("bot/$id/newOrderInfo");
        echo $content;
        die;
    }

    $content = '';
    function createTable($id, $file_name, $count_func)
    {
        $content = '';
        $file = "bot/$id/$file_name.csv";
        $csv = array_map('str_getcsv', file($file));
        if (!empty($csv)) {
            if ($_POST['file'] == 'Balance') {

                $json = file_get_contents("bot/$id/$id.json");
                $data = (json_decode($json));
                $exchange = $data->exchange;


                $count_balance = 0;
                $head_TI = false;
                $head_BA = false;
                $head_QA = false;
                $head_CA = false;
                $sign_flag = false;
                $tickerHeader = true;
                $BASEHeader = true ;
                $QUOTEHeader = true ;
                $totalBaseHeader = true;
                $totalQuoteHeader = true;
                $is_freezed = false;
                $is_revaluatedPrice = false;

                foreach ($csv as $k => $item) {
                    if ($item[0] == '+') {
                        if ($sign_flag == false) {
                            $count_balance = 0;
                        }
                        $count_balance++;
                        $sign_flag = true;
                    } else {
                        $sign_flag = false;
                        $content .= '<tr>';
                        if ($count_balance == 1) {
                            if (!$head_TI) {
                                $content .= '<tr>';
                                $content .= "<td class='th'> TICKER</td>";
                                for ($i = 0; $i <= 1; $i++) {
                                    $content .= "<td class='th'> </td>";
                                }
                                $content .= '</tr>';
                                $head_TI = true;
                            }
                            if ($head_TI) {
                                if ($tickerHeader) {
                                    foreach ($item as $key => $value) {
                                        $content .= "<td> $value</td>";
                                    }
                                    $tickerHeader = false;
                                } elseif (!$tickerHeader) {
                                    foreach ($item as $key => $value) {
                                        if ($key == 0) {
                                            $value = substr($value, 0, 10);
                                            $value = date("m-d H:i:s", $value);
                                        }
                                        $content .= "<td> $value</td>";
                                    }
                                }
                            }
                        } elseif ($count_balance == 2) {
                            if (!$head_BA) {
                                $content .= '<tr>';
                                $content .= "<td class='th'> BASE ACCOUNT </td>";
                                for ($i = 0; $i <= 18; $i++) {
                                    if($i == 0){
                                        $content .= "<td class='th'><div class='butt_block'>
                                        <button class='btn btn-info pull-right' id='base_pos' onclick='set.actionPos(\"BASE\")'>ok</button>
                                    </div></td>";
                                    }elseif($i == 10){
                                        $content .= "<td class='th'><input type='text' id='revInput_BASE' onkeyup='set.actionRevChange(\"BASE\")'></td>";
                                    }else{
                                        $content .= "<td class='th'> </td>";
                                    }
                                }
                                $content .= '</tr>';
                                $head_BA = true;
                            }
                            if ($head_BA) {
                                if ($BASEHeader) {
                                    $BASEHeader = false;
                                    foreach ($item as $kf => $value) {
                                        if($value == 'revaluatedPrice'){
                                            $is_revaluatedPrice = $kf;
                                        }elseif($value == 'isFreezed'){
                                            $is_freezed = $kf;
                                        }
                                        if ($value != '') $content .= "<td> $value</td>";
                                    }
                                }else{
                                    foreach ($item as $key => $value) {
                                        if($key == 0){
                                            if($exchange != 'df'){
                                                $value = (int)$value;
                                            }
                                            $value_ = "$value
                                                <label class='label-success_'> <input type='checkbox' class='form-check-input-BASE' data-id=$value> exchange </label>
                                                <label class='label-success_'> <input  type='checkbox' class='switch-check-input-BASE' data-id=$value> freeze </label>";
                                            $value = $value_;
                                        }
                                        if($is_freezed == $key){
                                            if($value == 1){
                                                $value = "<div class='isChecked'>$value</div><script>$('.isChecked').parent().parent().find('.switch-check-input-BASE').prop('checked', true);</script>";
                                            }
                                        }
                                        if($is_revaluatedPrice == $key){
                                            $value = "<div class='isRev-BASE' onclick='$(this).addClass(\"isRevaluatedPrice-BASE\");$(this).parent().prop(\"contenteditable\", true)'>$value</div>";

                                        }
                                        if ($value != '') $content .= "<td> $value</td>";
                                    }
                                }
                            }
                        } elseif ($count_balance == 3) {
                            if ($totalBaseHeader) {
                                $totalBaseHeader = false;
                            } elseif (!$totalBaseHeader) {
                                $content .= "<tr><td>TOTAL</td>";
                                foreach ($item as $value) {
                                    if ($value == '') {
                                        $content .= "<td> -eqOPPOSIDE $value</td>";
                                    } else {
                                        $content .= "<td> $value</td>";
                                    }
                                }

                            }
                        } elseif ($count_balance == 4) {
                            if (!$head_QA) {
                                $content .= '<tr>';
                                $content .= "<td class='th'> QUOTE_ACCOUNT </td>";
                                for ($i = 0; $i <= 18; $i++) {
                                    if($i == 0){
                                        $content .= "<td class='th'><div class='butt_block'>
                                        <button class='btn btn-info pull-right' id='quote_pos' onclick='set.actionPos(\"QUOTE\")'>ok</button>
                                    </div></td>";
                                    }elseif($i == 10){
                                        $content .= "<td class='th'><input type='text' id='revInput_QUOTE' onkeyup='set.actionRevChange(\"QUOTE\")'></td>";
                                    }else{
                                        $content .= "<td class='th'> </td>";
                                    }
                                }
                                $content .= '</tr>';
                                $head_QA = true;

                            }
                            if ($head_QA) {

                                if ($QUOTEHeader) {
                                    $QUOTEHeader = false;
                                    foreach ($item as $kf => $value) {
                                        if ($value != '') $content .= "<td> $value</td>";
                                        if($value == 'revaluatedPrice'){
                                            $is_revaluatedPrice = $kf;
                                        }elseif($value == 'isFreezed'){
                                            $is_freezed = $kf;
                                        }
                                    }
                                }else{
                                    foreach ($item as $key => $value) {
                                        if($key == 0){
                                            if($exchange != 'df'){
                                                $value = (int)$value;
                                            }
                                            $value_ = "$value
                                                <label class='label-success_'> <input type='checkbox' class='form-check-input-QUOTE' data-id=$value> exchange </label>
                                                <label class='label-success_'> <input  type='checkbox' class='switch-check-input-QUOTE' data-id=$value> freeze </label>";
                                            $value = $value_;
                                        }
                                        if($is_freezed == $key){
                                            if($value == 1){
                                                $value = "<div class='isChecked'>$value</div><script>$('.isChecked').parent().parent().find('.switch-check-input-QUOTE').prop('checked', true);</script>";
                                            }
                                        }

                                        if($is_revaluatedPrice == $key){
//                                            <i class='fas fa-pencil-alt pencil'></i>
                                            $value = "<div class='isRev-QUOTE' onclick='$(this).addClass(\"isRevaluatedPrice-QUOTE\");$(this).parent().prop(\"contenteditable\", true)'>$value </div>";

                                        }
                                        if ($value != '') $content .= "<td> $value</td>";
                                    }
                                }
                            }
                        } elseif ($count_balance == 5) {
                            if ($totalQuoteHeader) {
                                $totalQuoteHeader = false;
                            } elseif (!$totalQuoteHeader) {
                                $content .= "<tr><td>TOTAL</td>";
                                foreach ($item as $value) {
                                    if ($value == '') {
                                        $content .= "<td> -eqOPPOSIDE $value</td>";
                                    } else {
                                        $content .= "<td> $value</td>";
                                    }
                                }

                            }
                        }elseif($count_balance == 6 ){
                            if($head_CA != true){
                                $content .= '<tr>';
                                $content .= "<td class='th'> EQUITY</td>";
                                for ($i = 0; $i <= 8; $i++) {
                                    $content .= "<td class='th'> </td>";
                                }
                                $content .= '</tr>';
                                $head_CA = true;
                            }
                            if($head_CA){
                                foreach ($item as $value) {
                                    if ($value != '') $content .= "<td> $value</td>";
                                }
                            }
                        }

                    }
                }


                $content .= '</tr>';
            }
            elseif
            ($_POST['file'] == 'order'){
                $count_ = 0;
                $head_DI = false;
                $head_OP = false;
                $head_TO = false;
                foreach ($csv as $item) {
                    print_r($item);
                    if ($item[0] == '+') {
                        $count_++;
                    } else {
                        $checker = 1;
                        $content .= '<tr>';
                        foreach ($item as $key => $value) {
                            if ($value != '') {
                                if ($count_ == 1 && $head_DI != true) {
                                    $content .= '<tr>';
                                    $content .= "<td class='th'> DIRECT</td>";
                                    $content .= '</tr>';
                                    $content .= '<tr>';
                                    $content .= "<td>Side</th>";
                                    $content .= "<td>Order Type</td>";
                                    $content .= "<td>Amount</td>";
                                    $content .= "<td>Order Price</td>";
                                    $content .= '</tr>';
                                    $head_DI = true;
                                } elseif ($count_ == 2 && $head_OP != true) {
                                    $content .= '<tr>';
                                    $content .= "<td class='th'> OPPOSITE</td>";
                                    $content .= '</tr>';
                                    $content .= '<tr>';
                                    $content .= "<td>Opposite Account</td>";
                                    $content .= "<td>Type</td>";
                                    $content .= "<td>Executed Base</td>";
                                    $content .= "<td>Executed Quote</td>";
                                    $content .= "<td>Remaining Base</td>";
                                    $content .= "<td>Remaining Quote</td>";
                                    $content .= "<td>Total Base</td>";
                                    $content .= "<td>Total Quote</td>";
                                    $content .= '</tr>';
                                    $head_OP = true;
                                } elseif ($count_ == 3 && $head_TO != true) {
                                    $content .= '<tr>';
                                    $content .= "<td class='th'> TOTAL</td>";
                                    $content .= '</tr>';
                                    $content .= '<tr>';
                                    $content .= "<td>Opposite Account</td>";
                                    $content .= "<td>Type</td>";
                                    $content .= "<td>Executed Base</td>";
                                    $content .= "<td>Executed Quote</td>";
                                    $content .= "<td>Remaining Base</td>";
                                    $content .= "<td>Remaining Quote</td>";
                                    $content .= "<td>Total Base</td>";
                                    $content .= "<td>Total Quote</td>";
                                    $content .= '</tr>';
                                    $head_TO = true;
                                }
                                if ($checker == 1) {
                                    $content .= "<td> $value</td>";
                                }
                            }
                        }
                        $count_ = 0;
                        $content .= '</tr>';
                    }
                }
            } else {
                foreach ($csv as $item) {
                    $content .= '<tr>';
                    foreach ($item as $key => $value) {
                        $content .= "<td> $value</td>";
                    }
                    $content .= '</tr>';
                }
                $content .= '</tr>';
            }
        } elseif
        ($count_func < 1000 && empty($csv)) {
//                sleep(1);
            $count_func++;
            createTable($id, $file_name, $count_func);
        }
        return $content;
    }


    $content = createTable($id, $file_name, 1);
//    echo $content;
    print_r($content);


}