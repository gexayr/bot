<?php include './header.php'; ?>
<?php
//ini_set("display_errors",1);
//error_reporting(E_ALL);
$login = $_COOKIE['login'];
if($login != null){
    ?>
    <div class="container">
        <div class="row">
            <h1 style="text-align: center">New</h1>
        </div>
    </div>
<?php } ?>
<?

$dir    = $_SERVER['DOCUMENT_ROOT'] . '/new/bot/';
$files = scandir($dir);

array_shift($files);
array_shift($files);

///*GET USER*/

if(isset($_COOKIE['login'])){

    require "../pdo/config.php";

    try  {
        $connection = new PDO($dsn, $username, $password, $options);

        $sql = "SELECT * 
            FROM users
            WHERE login = :login";

        $login = $_COOKIE['login'];
        $password = $_COOKIE['key'];

        if($_COOKIE['key'] != md5(($_COOKIE['login']))) die();
        $statement = $connection->prepare($sql);
        $statement->bindParam(':login', $login, PDO::PARAM_STR);
        $statement->execute();

        $result = $statement->fetchAll();

        $allow_accounts = json_decode($result[0]['account']);
        $user_id = $result[0]['id'];

        $options_allow = '';
        foreach ($allow_accounts as $key=>$value) {
            $selected = '';
            if(isset($_GET['filter']) && $_GET['filter'] == $key){
                $selected = 'selected';
            }
            $allow[] = $key;

            $options_allow .= "<option value='$key' $selected>$key</option>";
        }

    } catch(PDOException $error) {
        echo ' <div class="alert alert-danger alert-dismissible fade in col-sm-6 col-sm-offset-3" style="text-align: center">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong> ' . $error->getMessage() . '
        </div>';
    }
}

///*END GET USER*/

?>


<div class="container">
    <div class="form-group" style="text-align: right">
        <?php
        $login = $_COOKIE['login'];
        if($login != null){
            ?>
            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" onclick="set.getTime()">
<!--<span  class="snow_img create_img" style="
    height: 75px;
    width: 152px;
    margin-left: -39px;
    margin-top: -37px;"
></span>-->
                Create New</button>
            <input type="hidden" id="user_id" value="<?=$user_id;?>">

        <?php } ?>
    </div>
    <table id="bots">
        <tr>
            <th style="width: 50px;">Name</th>
            <th style="text-align: right">
                <select name="acc_option" id="acc_option" class="acc_options" onchange="set.ChangeAccOption()">
                    <option value="">Account</option>
                    <?=$options_allow;?>
                </select>
            </th>
            <th style="text-align: right">Pair</th>
            <th style="text-align: right">Invest_Base</th>
            <th style="text-align: right">Invest_Quote</th>
            <th style="text-align: right">Type</th>
            <th style="text-align: right">Range</th>
            <th style="text-align: right">Status</th>
            <th style="text-align: right">Action</th>
        </tr>
        <button type="button" class="hidden" id="button_open" data-toggle="modal" data-target="#myModalOpen"></button>

        <?php
        function getTicker($exchange, $pair, $counter){
            $dir_ticker = '/home/talisant/tickers/' . $exchange . '/';
            $in_ticker_folder = scandir($dir_ticker . $pair);
            if (in_array("ticker.json", $in_ticker_folder)) {
                $str = file_get_contents($dir_ticker . $pair . '/ticker.json');
                if($str == '' && $counter < 40){
                    sleep(1);
                    $counter++;
                    getTicker($exchange, $pair, $counter);
                }else{
                    return $str;
                }
            }
        }
//        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//
        $comand_all =  shell_exec("ps -ef | grep talisant/Surfer_final" );
        $comand_all_arr = explode("\n", $comand_all);
        $comand_all_tick =  shell_exec("ps -ef | grep talisant/tickers/" );
        $comand_all_arr_tickers = explode("\n", $comand_all_tick);

        foreach($comand_all_arr as $item){

            $pos_edit = strpos($item, 'mainAlgorithm1');
            $pos_order = strpos($item, 'mainMarket');

            if($pos_edit != false){
                $comand_all_arr_edit[] = $item;
            }
            if($pos_order != false){
                $comand_all_arr_order[] = $item;
            }

        }

//        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

        foreach ($files as $folder) {
            $dir_    = "$dir/$folder";
            $files_ = scandir($dir_);
            array_shift($files_);
            array_shift($files_);
            $style = 'btn-primary';
            $pause_class = "btn-info";
            $pause_content = "Pause";


            if (!in_array("remove", $files_)) {


                if (in_array("pauseA", $files_)) {
                    $pause_class = "btn-success";
                    $pause_content = "Activate";
                }
                $json = file_get_contents("bot/$folder/$folder.json");
                $data = (json_decode($json));
                $account = $data->account;

                if (in_array($account, $allow)) {

                    if(isset($_GET['filter']) && $_GET['filter'] != '' && $_GET['filter'] != $account){
                        continue;
                    } else {

                        $pair = $data->pair;
                        $exchange = $data->exchange;

//      ================================================================================
//                        OUT OF RANGE
//      ================================================================================
                        $out_of_range = '';

                        if (!in_array("stop", $files_)) {

//                            $dir_ticker = '/home/talisant/tickers/' . $exchange . '/';
//                            $in_ticker_folder = scandir($dir_ticker . $pair);
//                            if (in_array("ticker.json", $in_ticker_folder)) {
//                                $str = file_get_contents($dir_ticker . $pair . '/ticker.json');
//                            }

                            $str = getTicker($exchange, $pair, 1);

                            $json = json_decode($str, true);

                            if($json == null){
                                $str = getTicker($exchange, $pair, 1);
                                $json = json_decode($str, true);
                            }
                            $ask = $json['ask'];
                            $bid = $json['bid'];

                            if ($bid > $data->maxRange || $ask < $data->minRange) {
                                $out_of_range = "<span class='fa fa-power-off red-icon out-of' data-toggle='tooltip' data-placement='top' title='Out Of Range'></span> ";
                            }
                        }
//      ================================================================================
//                        END OUT OF RANGE
//      ================================================================================



                        $range = $data->minRange . '-' . $data->maxRange;

                        if (in_array("stopA", $files_)) {
                            $style = 'btn-danger';
                        }
                        if (in_array("remove", $files_)){
                            $style = "btn-warning";
                        }
                        ?>

                        <tr>

                            <td onclick="set.actionOpen('<?= $folder ?>')"><a
                                        href="javascript: void(0)"><? echo date("m-d H:i:s", ($folder + 4 * 3600));
                                    echo " (" . $folder . ")"; ?></a></td>
                            <td><?= $account; ?></td>
                            <td><?= $pair; ?></td>
                            <td><?= $data->investBase; ?></td>
                            <td><?= $data->investQuote; ?></td>
                            <td><?= $data->type.$data->earn; ?></td>
                            <td><?= $range; ?></td>
                            <td>
                                <div class="check_light">
                    <?php
                        if (!in_array("stopA", $files_)) {
//      =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//                        CHECK LIGHT
//      =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                            $status_color = 'red';
                            foreach ($comand_all_arr_tickers as $item) {
                                $pos_tick = strpos($item, $exchange . ' ' . $pair);
//                                if ($exchange == 'df') {
//                                    $pos_tick = strpos($item, $exchange);
//                                }
                                if ($pos_tick != false) {
                                    $status_color = 'green';
                                    break;
                                }
                            }
                            echo "<span class='fa fa-power-off $status_color-icon' data-toggle='tooltip' data-placement='top' title='Ticker'></span> ";
//                            echo "<span class='circle $status_color' data-toggle='tooltip' data-placement='top' title='Ticker'></span> ";


                            $status_color = 'red';
                            foreach ($comand_all_arr_edit as $item) {
                                $pos_edit = strpos($item, $folder);
                                if ($pos_edit != false) {
                                    $status_color = 'green';
                                    break;
                                }
                            }
                            if($status_color == 'green') {
                                echo "<span class='fa fa-power-off $status_color-icon' data-toggle='tooltip' data-placement='top' title='mainAlgorithm1'></span>";
//                                echo "<span class='circle $status_color' data-toggle='tooltip' data-placement='top' title='mainAlgorithm1'></span>";
                            }else{
                                echo "<span class='fa fa-power-off $status_color-icon' data-toggle='tooltip' data-placement='top' title='algo' onclick=\"reRun('$data->account', '$folder', '$data->exchange', '$data->pair', 'algo')\" style='cursor: pointer'></span> ";
//                                echo "<span class='circle $status_color' data-toggle='tooltip' data-placement='top' title='algo' onclick=\"reRun('$data->account', '$folder', '$data->exchange', '$data->pair', 'algo')\" style='cursor: pointer'></span> ";
                            }

                            $status_color = 'red';
                            foreach ($comand_all_arr_order as $item) {
                                $pos_order = strpos($item, $folder);
                                if ($pos_order != false) {
                                    $status_color = 'green';
                                    break;
                                }
                            }
                            if($status_color == 'green'){
                                echo "<span class='fa fa-power-off $status_color-icon' data-toggle='tooltip' data-placement='top' title='mainMarket'></span> ";
//                                echo "<span class='circle $status_color' data-toggle='tooltip' data-placement='top' title='mainMarket'></span> ";
                            }else{
                                echo "<span class='fa fa-power-off $status_color-icon' data-toggle='tooltip' data-placement='top' title='market' onclick=\"reRun('$data->account', '$folder', '$data->exchange', '$data->pair', 'market')\" style='cursor: pointer'></span> ";
//                                echo "<span class='circle $status_color' data-toggle='tooltip' data-placement='top' title='market' onclick=\"reRun('$data->account', '$folder', '$data->exchange', '$data->pair', 'market')\" style='cursor: pointer'></span> ";
                            }

//      =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//                        END CHECK LIGHT
//      =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                        }
                    ?>
                                </div>
                            </td>
                            <td id="bot_<?= $folder ?>" style="text-align: right">

                                <?= $out_of_range; ?>
                                <button type="button" class="btn btn-info" data-toggle="modal"
                                        data-target="#myModalEdit"
                                        onclick="set.actionEdit(<?= $folder ?>)">
<!--<span class="snow_img button_img" style="
    background-image: url(/img/1237.png);
    height: 75px;
    width: 54px;
    margin-left: -15px;
    margin-top: -14px;"
></span>-->
                                    Edit
                                </button>
                                <button type="button" class="btn <?= $pause_class; ?> "
                                        onclick="set.actionPause(<?= $folder ?>, this)">
<!--<span class="snow_img button_img" style="
    background-image: url(/img/1237.png);
    height: 75px;
    width: 75px;
    margin-left: -18px;
    margin-top: -17px;"
></span>-->
                                    <?= $pause_content; ?>
                                </button>

                                <?php
                                    if($style != 'btn-danger' && $style != 'btn-warning'){
                                ?>
                                <button class="btn btn-info" onclick="set.stopButton_(<?= $folder ?>, this)">Stop</button>
                                <?php } ?>
                                <button type="button" class="btn <?= $style; ?>"
                                        onclick="set.actionRemove(<?= $folder ?>, this)">
<!--<span class="snow_img button_img" style="
    background-image: url(/img/1236.png);
    height: 75px;
    width: 94px;
    margin-left: -18px;
    margin-top: -27px;"
></span>-->
                                    Remove
                                </button>
                            </td>
                        </tr>

                        <?php
                    }
                }
            }
        }
        ?>
    </table>

    <!-- Modal -->

    <div class="modal fade" id="myModalOpen" role="dialog" style="text-align: left;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <!-- OPEN MODAL-->
            <div class="modal-content" style="display: table-row-group">
                <div class="modal-header" style="border: none">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="open_modal_header"></h4>
                    <div id="ticker_result"></div>

                </div>

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#menu1">FlowSheet</a></li>
                    <li><a data-toggle="tab" href="#menu2">Balance</a></li>
                    <li><a data-toggle="tab" href="#menu3">InvoicesSheet</a></li>
                    <li><a data-toggle="tab" href="#menu4">order</a></li>
                    <li><a data-toggle="tab" href="#menu5">newOrderInfo</a></li>
                </ul>

                <div class="tab-content">

                    <div id="menu1" class="tab-pane fade in active">
                        <div class="modal-body" style="padding-left: 30px;">
                            <table class="bots" id="FlowSheet_table">
                            </table>
                            <a style="margin-top: 15px;" class="btn btn-info" onclick="set.downloadButton('FlowSheet')">Export as CSV</a>
                        </div>
                    </div>


                    <div id="menu2" class="tab-pane fade">
                        <div class="modal-body" style="padding-left: 30px;">
                            <table id="Balance_table" class="bots">
                            </table>
                            <input value="Export as CSV" style="margin-top: 15px;" type="button" class="btn btn-info"
                                   onclick="$('#Balance_table').table2CSV({header:['']}, '_Balance')">
                        </div>
                    </div>


                    <div id="menu3" class="tab-pane fade">
                        <div class="modal-body" style="padding-left: 30px;">
                            <table class="bots">
                                <tbody id="InvoicesSheet_table">
                                </tbody>
                            </table>
                            <a class="btn btn-info" style="margin-top: 15px;"  onclick="set.downloadButton('InvoicesSheet')">Export as CSV</a>
                        </div>
                    </div>


                    <div id="menu4" class="tab-pane fade">
                        <div class="modal-body" style="padding-left: 30px;">
                            <table class="bots" id="order_table">
                            </table>
                            <input value="Export as CSV" style="margin-top: 15px;" type="button" class="btn btn-info"
                                   onclick="$('#order_table').table2CSV({header:['']}, '_order')"></div>
                    </div>


                    <div id="menu5" class="tab-pane fade">
                        <div class="modal-body" style="padding-left: 30px;">
                            <div class="bots" id="newOrderInfo">
                            </div>
                        </div>
                    </div>


                </div>

                <input type="hidden" id="open_bot">
                <div class="modal-footer">
<!--                    <a type="submit" class="btn btn-info" style="float:left;"  onclick="set.downloadButton('userSchedule')">Download Schedule</a>-->
<!--                    <button class="btn btn-danger" id="close_position" onclick="set.lineClose(this)">close positions</button>-->
                    <button type="button" id="close_modal" class="btn btn-secondary" data-dismiss="modal">Cancel
                    </button>

                </div>
            </div>
            <!--END OPEN MODAL-->

        </div>
    </div>

    <div class="modal fade" id="myModal" role="dialog" style="text-align: left;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <!-- SETTINGS JSON-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!--                <form action="result.php" method="post">-->
                <form action="javascript: void(0)">

                    <div class="modal-body">


                        <div id="step_1">


                            <div class="form-group"> <!-- Full Name -->
                                <label for="name_id" class="control-label">ID</label>
                                <input type="text" class="form-control" id="name_id" required name="id" value="<?= time(); ?>"
                                       readonly>
                            </div>



                            <div class="form-group">
                                <label for="account" class="control-label">Exchange</label>
                                <select class="form-control" id="exchange" required name="Exchange" onchange="set.GetExchange()">
                                    <option value="">select...</option>

                                    <option value="binance">Binance</option>
<!--                                    <option value="bitfinex">Bitfinex</option>-->
                                    <option value="df">DF</option>

                                </select>
                            </div>



                            <div class="form-group"> <!-- State Button -->
                                <label for="account" class="control-label">Account</label>
                                <select class="form-control" id="account" required name="Account">
                                    <option value="">select...</option>

                                </select>
                            </div>

                            <div class="form-group">
                                <label for="pair" class="control-label">Pair</label>
                                <select class="form-control" id="pair" required name="pair" onchange="set.ChangePair()">
                                    <option value="">select...</option>
                                </select>
                            </div>

                            <hr>
                            <div class="form-group" style="text-align: right">
                                <a class="btn btn-info" id="next">Next</a>
                            </div>

                        </div>

                        <div id="step_2">


                            <div class="form-group">
                                <label for="earn" class="control-label">Earn</label>
                                <select class="form-control" id="earn" required name="earn">
                                    <option value="BASE" id="BASE">BASE</option>
                                    <option value="QUOTE" id="QUOTE">QUOTE</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="add_asset" class="control-label">Investment BASE</label>
                                <input type="text" class="form-control" id="invest_base" required name="invest_base"
                                       placeholder="BASE">
                            </div>
                            <div class="form-group">
                                <label for="add_asset" class="control-label">Investment QUOTE</label>
                                <input type="text" class="form-control" id="invest_quote" required name="invest_quote"
                                       placeholder="QUOTE">
                            </div>

                            <div class="form-group">
                                <label for="earn" class="control-label">Algorithm Type</label>
                                <select class="form-control" id="type" required name="type">
                                    <option value="surfer" id="surfer">Surfer</option>
                                    <option value="modInc" id="modInc">modInc</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="upper_threshold" class="control-label">maxRange</label>
                                <input type="text" class="form-control" id="maxRange"
                                       required name="upper_threshold">
                            </div>

                            <div class="form-group">
                                <label for="lower_threshold" class="control-label">minRange</label>
                                <input type="text" class="form-control" id="minRange"
                                       required name="lower_threshold">
                            </div>

                            <div class="form-group">
                                <label for="takerFee" class="control-label">takerFee</label>
                                <input type="text" class="form-control" id="takerFee" required name="takerFee">
                            </div>
                            <div class="form-group">
                                <label for="makerFee" class="control-label">makerFee</label>
                                <input type="text" class="form-control" id="makerFee" required name="makerFee">
                            </div>




                            <div class="form-group">
                                <label for="BaseMinLot" class="control-label">BaseMinLot</label>
                                <input type="text" class="form-control" id="BaseMinLot" required name="BaseMinLot">
                            </div>

                            <div class="form-group">
                                <label for="QuoteMinLot" class="control-label">QuoteMinLot</label>
                                <input type="text" class="form-control" id="QuoteMinLot" required name="QuothMinLot">
                            </div>

                            <div class="form-group">
                                <label for="coef_lambda" class="control-label">minLot</label>
                                <input type="text" class="form-control" id="minLot"
                                       required name="minLot">
                            </div>

                            <div class="form-group">
                                <label for="lambda" class="control-label">lambda</label>
                                <input type="text" class="form-control" id="lambda" required name="lambda"
                                       placeholder="λ">
                            </div>

                            <div class="form-group">
                                <label for="spread" class="control-label">Spread %</label>
                                <input type="text" class="form-control" id="spread" required name="spread"
                                       placeholder="%">
                            </div>



                            <div class="form-group">
                                <label for="minRRTrailingStop" class="control-label">minRR Trailing Stop</label>
                                <input type="text" class="form-control" id="minRRTrailingStop" required name="minRRTrailingStop">
                            </div>



                            <div class="form-group">
                                <label for="distance" class="control-label">distance Trailing Stop </label>
                                <input type="text" class="form-control" id="distanceTrailingStop" required name="distanceTrailingStop">
                            </div>




                            <div class="form-group">
                                <label for="forceMajor" class="control-label">forceMajor</label>
                                <select class="form-control" id="forceMajor" disabled>
                                    <option value="0">0</option>
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="minRRAlgorithm" class="control-label">minRR Algorithm</label>
                                <input type="text" class="form-control" id="minRRAlgorithm" required name="minRRAlgorithm">
                            </div>



                            <div class="form-group">
                                <label for="distanceAlgorithm" class="control-label">distance Algorithm</label>
                                <input type="text" class="form-control" id="distanceAlgorithm" required name="distanceAlgorithm">
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer" id="modal_footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-info" id="send">Send</button>
                    </div>

                </form>

            </div>
            <!--END SETTINGS JSON-->

        </div>
    </div>


    <div class="modal fade" id="myModalEdit" role="dialog" style="text-align: left;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <!-- EDIT SETTINGS-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit</h4>
                </div>
                <form action="javascript: void(0)">

                    <div class="modal-body">

                        <div class="form-group"> <!-- Full Name -->
                            <label for="name_id" class="control-label">ID</label>
                            <input type="text" class="form-control" id="name_id_edit" readonly name="id" value="<?= time(); ?>" readonly>
                        </div>

                        <div class="form-group">
                            <label for="account" class="control-label">Exchange</label>
                            <input type="text" class="form-control" id="exchange_edit" readonly>
                        </div>

                        <div class="form-group"> <!-- State Button -->
                            <label for="account" class="control-label">Account</label>
                            <input type="text" class="form-control" id="account_edit" readonly>
                        </div>

                        <div class="form-group">
                            <label for="pair" class="control-label">Pair</label>
                            <input type="text" class="form-control" id="pair_edit" readonly>
                        </div>

                        <div class="form-group">
                            <label for="earn" class="control-label">Earn</label>
                            <select class="form-control" id="earn_edit" required>
                                <option value="BASE" id="BASE">BASE</option>
                                <option value="QUOTE" id="QUOTE">QUOTE</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="add_asset" class="control-label">Investment BASE</label>
                            <input type="text" class="form-control" id="invest_base_edit" readonly>
                        </div>
                        <div class="form-group">
                            <label for="add_asset" class="control-label">Investment QUOTE</label>
                            <input type="text" class="form-control" id="invest_quote_edit" readonly>
                        </div>

                        <div class="form-group">
                            <label for="add_asset" class="control-label">Add Investment BASE</label>
                            <input type="text" class="form-control" id="add_invest_base_edit" placeholder="Add BASE">
                        </div>
                        <div class="form-group">
                            <label for="add_asset" class="control-label">Add Investment QUOTE</label>
                            <input type="text" class="form-control" id="add_invest_quote_edit" placeholder="Add QUOTE">
                        </div>

                        <div class="form-group">
                            <label for="earn" class="control-label">Alorithm Type</label>
                            <select class="form-control" id="type_edit" required>
                                <option value="surfer" id="surfer">Surfer</option>
                                <option value="modInc" id="modInc">modInc</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="upper_threshold" class="control-label">maxRange</label>
                            <input type="text" class="form-control" id="maxRange_edit">
                        </div>

                        <div class="form-group">
                            <label for="lower_threshold" class="control-label">minRange</label>
                            <input type="text" class="form-control" id="minRange_edit">
                        </div>

                        <div class="form-group">
                            <label for="takerFee" class="control-label">takerFee</label>
                            <input type="text" class="form-control" id="takerFee_edit">
                        </div>
                        <div class="form-group">
                            <label for="makerFee" class="control-label">makerFee</label>
                            <input type="text" class="form-control" id="makerFee_edit">
                        </div>

                        <div class="form-group">
                            <label for="BaseMinLot" class="control-label">BaseMinLot</label>
                            <input type="text" class="form-control" id="BaseMinLot_edit">
                        </div>

                        <div class="form-group">
                            <label for="QuoteMinLot" class="control-label">QuoteMinLot</label>
                            <input type="text" class="form-control" id="QuoteMinLot_edit">
                        </div>

                        <div class="form-group">
                            <label for="coef_lambda" class="control-label">minLot</label>
                            <input type="text" class="form-control" id="minLot_edit"
                                   required name="minLot">
                        </div>

                        <div class="form-group">
                            <label for="lambda" class="control-label">lambda</label>
                            <input type="text" class="form-control" id="lambda_edit">
                        </div>

                        <div class="form-group">
                            <label for="spread" class="control-label">Spread %</label>
                            <input type="text" class="form-control" id="spread_edit">
                        </div>



                        <div class="form-group">
                            <label for="minRRTrailingStop" class="control-label">minRR Trailing Stop</label>
                            <input type="text" class="form-control" id="minRRTrailingStop_edit">
                        </div>



                        <div class="form-group">
                            <label for="distance" class="control-label">distance Trailing Stop </label>
                            <input type="text" class="form-control" id="distanceTrailingStop_edit">
                        </div>


<!---->
<!--                        <div class="form-group">-->
<!--                            <label for="forceMajor" class="control-label">forceMajor</label>-->
<!--                            <input type="text" class="form-control" id="forceMajor_edit">-->
<!--                        </div>-->


                        <div class="form-group">
                            <label for="forceMajor_edit" class="control-label">forceMajor</label>
                            <select class="form-control" id="forceMajor_edit">
                                <option value="0">0</option>
                                <option value="1">1</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="minRRAlgorithm" class="control-label">minRR Algorithm</label>
                            <input type="text" class="form-control" id="minRRAlgorithm_edit">
                        </div>



                        <div class="form-group">
                            <label for="distanceAlgorithm" class="control-label">distance Algorithm</label>
                            <input type="text" class="form-control" id="distanceAlgorithm_edit">
                        </div>

                    </div>

                    <div class="modal-footer" id="hide_for_copy">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <a type="submit" class="btn btn-info" onclick="set.copyButton('<?=$user_id;?>')" title='Create New Bot With Same Parameters'>Create Copy</a>
                        <a type="submit" class="btn btn-info" onclick="set.stopButton(this)">Stop</a>
                        <button type="submit" class="btn btn-success" onclick="set.saveButton()">Save</button>
                    </div>

                </form>

            </div>
            <!-- END EDIT SETTINGS-->
        </div>
    </div>


</div>

<?php include './footer.php'; ?>
