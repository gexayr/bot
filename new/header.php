<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta required name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>New App</title>
    <!-- Latest compiled and minified CSS -->
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <link rel="icon" type="image/png" sizes="32x32" href="../favicon.ico">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">

    <!-- fa-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="../styles.css">
    <script src="/csv2array.js"></script>
    <script src="html2CSV.js"></script>
    <style>
        /** {*/
            /*margin: 0;*/
            /*padding: 0;*/
        /*}*/
        /*body {*/
            /*!*background-color: #18191f;*!*/
            /*background-color: #1c2935;*/
            /*background-image: url('/img/snow.png'), url('/img/snow3.png'), url('/img/snow2.png');*/
            /*-webkit-animation: snow 20s linear infinite;*/
            /*-moz-animation: snow 20s linear infinite;*/
            /*-ms-animation: snow 20s linear infinite;*/
            /*animation: snow 20s linear infinite;*/
        /*}*/

        /*@keyframes snow {*/
            /*0% {background-position: 0px 0px, 0px 0px, 0px 0px;}*/
            /*100% {background-position: 500px 1000px, 400px 400px, 300px 300px;}*/
        /*}*/

        /*@-moz-keyframes snow {*/
            /*0% {background-position: 0px 0px, 0px 0px, 0px 0px;}*/
            /*100% {background-position: 500px 1000px, 400px 400px, 300px 300px;}*/
        /*}*/

        /*@-webkit-keyframes snow {*/
            /*0% {background-position: 0px 0px, 0px 0px, 0px 0px;}*/
            /*50% {background-color:#b4cfe0;}*/
            /*100% {background-position: 500px 1000px, 400px 400px, 300px 300px; background-color:#18191f;}*/
        /*}*/

        /*@-ms-keyframes snow {*/
            /*0% {background-position: 0px 0px, 0px 0px, 0px 0px;}*/
            /*100% {background-position: 500px 1000px, 400px 400px, 300px 300px;}*/
        /*}*/

        /*h2 {*/
            /*width: 400px;*/
            /*text-align: center;*/
            /*color: white;*/
            /*font: 50px/1 'Spirax', cursive;*/
            /*text-shadow: 0px 0px 4px rgba(255,255,255, 0.5);*/
            /*position: absolute;*/
            /*top: 40%;*/
            /*left: 50%;*/
            /*transform: translate(-50%, 50%);*/
        /*}*/
    </style>

</head>
<?php
$body_class = 'lightSwitch';
if($_COOKIE['mode'] == 'night') {
    $body_class = 'darkSwitch';
}
?>
<body class="<?=$body_class;?>">
<div class="bg"> <!-- <img src="/images/bg_blue.png"></img> --></div>

<nav class="navbar navbar-default">
    <div class="container-fluid" >

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">HOME</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <!--                <li><a href="/">Home</a></li>-->
                <?php
                if($_COOKIE['login'] == null) {
                    ?>
                    <li><a href="../login">Login</a></li>
                    <li><a href="../login/reg.php">Registration</a></li>

                <?php } else{ ?>
                    <li><a href="/">Old</a></li>
                    <li><a href="../login/logout.php">Logout (<?=$_COOKIE['login']?>)</a></li>

                <?php } ?>
                <?php
                $night_mode = 'night';
                if($_COOKIE['mode'] == 'day') {
                    $night_mode = 'night';
                }
                ?>
                <div class="theme_switch" id="theme_switch" onclick="<?=$night_mode;?>();">☀</div>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

