<?php

ini_set("display_errors",1);
error_reporting(E_ALL);
if(isset($_POST['id'])) {



    if(isset($_POST['user_fees']) && $_POST['user_fees'] == true){
        $takerFee = $_POST['takerFee'];
        $makerFee = $_POST['makerFee'];
        $str_set = "$takerFee,$makerFee";
        file_put_contents('bot/' . $_POST['id'] . "/UserFees.csv", $str_set);
        file_put_contents('bot/' . $_POST['id'] . "/" . 'redUserFees', date("Y-m-d H-i-s") . "\n", FILE_APPEND);
        chmod('bot/' . $_POST['id'] . "/" . 'redUserFees', 0777);
        chmod('bot/' . $_POST['id'] . "/" . 'UserFees.csv', 0777);
        unset($_POST['user_fees']);

    }

    if(isset($_POST['add_asset']) && $_POST['add_asset'] == true){
        $investBase = $_POST['add_invest_base'];
        $investQuote = $_POST['add_invest_quote'];
        if($investBase == ''){
            $investBase = 0;
        }
        if($investQuote == ''){
            $investQuote = 0;
        }
        $str_set = "$investBase,$investQuote";
        file_put_contents('bot/' . $_POST['id'] . "/AddAsset.csv", $str_set);
        file_put_contents('bot/' . $_POST['id'] . "/" . 'redAddAsset', date("Y-m-d H-i-s") . "\n", FILE_APPEND);
        chmod('bot/' . $_POST['id'] . "/" . 'redAddAsset', 0777);
        chmod('bot/' . $_POST['id'] . "/" . 'AddAsset.csv', 0777);
        unset($_POST['add_asset']);

    }

    if(isset($_POST['algo_params']) && $_POST['algo_params'] == true){

        $type = $_POST['type'] . $_POST['earn'];
        $minRange = $_POST['minRange'];
        $maxRange = $_POST['maxRange'];
        $lambda = $_POST['lambda'];
        $QUOTEMinLot = $_POST['QuoteMinLot'];
        $BASEMinLot = $_POST['BaseMinLot'];
        $minLot = $_POST['minLot'];
        $spread = $_POST['spread'];
        $minRRAlgorithm = $_POST['minRRAlgorithm'];
        $distanceAlgorithm = $_POST['distanceAlgorithm'];
        $forceMajor = $_POST['forceMajor'];
        $str_set = "$type,$minRange,$maxRange,$lambda,$QUOTEMinLot,$BASEMinLot,$minLot,$spread,$minRRAlgorithm,$distanceAlgorithm,$forceMajor";
        file_put_contents('bot/' . $_POST['id'] . "/AlgoParams.csv", $str_set);
        file_put_contents('bot/' . $_POST['id'] . "/" . 'redAlgoParams', date("Y-m-d H-i-s") . "\n", FILE_APPEND);
        chmod('bot/' . $_POST['id'] . "/" . 'redAlgoParams', 0777);
        chmod('bot/' . $_POST['id'] . "/" . 'AlgoParams.csv', 0777);
        unset($_POST['algo_params']);

    }

    if(isset($_POST['TS_Params']) && $_POST['TS_Params'] == true){

        $minRRTrailingStop = $_POST['minRRTrailingStop'];
        $distanceTrailingStop = $_POST['distanceTrailingStop'];
        $str_set = "$minRRTrailingStop,$distanceTrailingStop";
        file_put_contents('bot/' . $_POST['id'] . "/TSParams.csv", $str_set);
        file_put_contents('bot/' . $_POST['id'] . "/" . 'redTSParams', date("Y-m-d H-i-s") . "\n", FILE_APPEND);
        chmod('bot/' . $_POST['id'] . "/" . 'TSParams.csv', 0777);
        chmod('bot/' . $_POST['id'] . "/" . 'redTSParams', 0777);
        unset($_POST['TS_Params']);

    }

    $_POST['investBase'] = $_POST['add_invest_base'] + $_POST['investBase'];
    $_POST['investQuote'] = $_POST['add_invest_quote'] + $_POST['investQuote'];
    unset($_POST['add_invest_base']);
    unset($_POST['add_invest_quote']);
    $arr_new = $_POST;
    $str = "{\n";

    foreach ($arr_new as $key => $val) {
        $str .= '"' . $key . '"' . ':' . '"' . $val . '"' . ",\n";
    }

    $str = substr($str, 0, -2);

    file_put_contents('bot/' . $_POST['id'] . "/" . $_POST['id'] . '.json', $str . "\n}" . "\n");
    chmod('bot/' . $_POST['id'] . "/" . $_POST['id'] . '.json', 0777);



}

echo('success');