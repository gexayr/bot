var set = {
    ajaxMethod: 'POST',



    actionX: function(id,quoteId) {

        var formData = new FormData();

        formData.append('id', id);
        formData.append('quote_id', quoteId);
        console.log(id);
        $.ajax({
            url: 'us_int.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                console.log(result);
                // $("#selectCity").html(result);
            }
        });
    },

    ChangeAccOption: function() {

        var option = ($('#acc_option').val());

        console.log(option);
        window.location.href = '/new/?filter=' + option;

    },

    ChangePair: function() {

        var pair = ($('#pair').val());

        var part_1 = pair.substring(0, 3);
        var part_2 = pair.substring(3, 6);
        if(pair == 'DASHUSD' || pair == 'DASHEUR'){
            var part_1 = pair.substring(0, 4);
            var part_2 = pair.substring(4, 7);
        }else if(pair == 'BCHABCUSD' || pair == 'BCHABCBTC' || pair == 'BCHABCUSDT'){
            var part_1 = pair.substring(0, 6);
            var part_2 = pair.substring(6, 9);
        }else if(pair == 'BCHSVUSD' || pair == 'BCHSVBTC' || pair == 'BCHSVUSDT'){
            var part_1 = pair.substring(0, 5);
            var part_2 = pair.substring(5, 9);
        }else if(pair == 'BCHABC-USDT'){
            var part_1 = pair.substring(0, 5);
            var part_2 = pair.substring(7, 9);
        }else if(pair == 'BCHSV-USDT'){
            var part_1 = pair.substring(0, 5);
            var part_2 = pair.substring(7, 15);
        }

        $('#Y').text(part_1);
        $('#X').text(part_2);
    },

    GetExchange: function() {

        var formData = new FormData();
        var exchange = ($('#exchange').val());
        formData.append('exchange', exchange);
        $.ajax({
            url: '/account.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                $('#account').html(result);
            }
        });
        $.ajax({
            url: '/tickers.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                $('#pair').html(result);
            }
        });
    },


    actionOpen: function (id) {
        // console.log('open ' + id);
        $('#button_open').click();
        $('#open_modal_header').text(id);
        $('#edit_stop_button').text(id);
        $('#open_bot').val(id);

        var count_bal = 1;
        // var countFlowSheet = 1;
        // var countInvoicesSheet = 1;
        // var countOrderTable = 1;
        var loader = "<div class='loader_block'><i class='fas fa-sync fa-spin'></i></div>";
        var count = 1;
        var html ='';
        $('#Balance_table').html(loader);
        $('#FlowSheet_table').html(loader);
        $('#InvoicesSheet_table').html(loader);
        $('#order_table').html(loader);
        $('#newOrderInfo').html(loader);
        var formData = new FormData();
        formData.append('id', id);


        show_BalanceTab(count_bal);
        function show_BalanceTab(count) {
            formData.append('file', 'Balance');
            $.ajax({
                url: '/new/csv_read.php',
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {

                },
                success: function (result) {
                    // console.log(result);
                    // html = result;
                    if (result == '') {
                        console.log('balanceTab not load '+ count);
                        if(count < 80){
                            count++;
                            show_BalanceTab(count)
                        }
                    }else{
                        $('#Balance_table').html(result);
                    }
                }
            });

        }


        show_FlowSheetTab(count);
        function show_FlowSheetTab(count) {
            formData.append('file', 'FlowSheet');

            $.ajax({
                url: '/new/csv_read.php',
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {

                },
                success: function (result) {
                    if (result == '') {
                        console.log('FlowSheetTab not load ' + count);
                        if (count < 50) {
                            count++;
                            show_FlowSheetTab(count)
                        }
                    }else{
                        $('#FlowSheet_table').html(result);
                    }
                }
            });
        }


        show_InvoicesSheetTab(count);
        function show_InvoicesSheetTab(count) {
            formData.append('file', 'InvoicesSheet');

            $.ajax({
                url: '/new/csv_read.php',
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {

                },
                success: function (result) {
                    if (result == '') {
                        console.log('InvoicesSheetTab not load ' + count);
                        if (count < 50) {
                            count++;
                            show_InvoicesSheetTab(count)
                        }
                    }else{
                        $('#InvoicesSheet_table').html(result);
                    }
                }
            });
        }

        show_OrderTableTab(count);
        function show_OrderTableTab(count) {
            formData.append('file', 'order');

            $.ajax({
                url: '/new/csv_read.php',
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {

                },
                success: function (result) {
                    if (result == '') {
                        console.log('OrderTable not load ' + count);
                        if (count < 10) {
                            count++;
                            show_InvoicesSheetTab(count)
                        }
                    }else{

                        $('#order_table').html(result);
                    }
                }
            });
        }

        formData.append('file', 'newOrderInfo');
        $.ajax({
            url: 'csv_read.php',
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {

            },
            success: function (result) {
                if (result == '') {
                    console.log('newOrderInfo not load ' + count);
                }else{
                    $('#newOrderInfo').html(result);
                }
            }
        });
        /*GET TICKERS*/
        var exchange = '';
        var pair = '';


        $.ajax({
            url: 'bot/'+ id + '/' + id + '.json',
            data: '',
            cache: false,
            processData: false,
            contentType: false,
            success: function( result ) {
                // console.log(result);
                exchange = result.exchange;
                pair = result.pair;


                formData.append('exchange_', exchange);
                formData.append('pair', pair);
                getTickers(exchange,pair,count);

            }
        });

        function getTickers(exchange, pair, count) {
            $.ajax({
                url: '/tickers.php',
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {
                },
                success: function (result) {
                    // console.log(result);
                    if (result == "<div class='tickers'><span>ask:</span> <span> bid:</span>  <span>Date:</span> 01-01 04:00:00</div>") {
                        if (count < 10) {
                            console.log('getTick ' + count);
                            count++;
                            getTickers(exchange, pair, count);
                        }
                    }
                    html = result;
                    $('#ticker_result').html(html);
                }
            });
        }
        /*END GET TICKERS*/

    },


    actionRemove: function (id, element) {

        if (!confirm("Are you sure ?")) {
            return false
        }

        var rem_element = $(element).parent().parent();
        rem_element.fadeOut();
        $('#myModalOpen').hide();
        // console.log('remove ' + id);

        var formData = new FormData();
        formData.append('id', id);
        $.ajax({
            url: 'remove.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function(result){
                // console.log(result);
            }
        });
    },

    getTime: function() {

        $.ajax({
            url: '/time.php',
            type: this.ajaxMethod,
            data: '',
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                if ( $('#step_1').attr('style') != 'display: none;' ) {
                    $('#name_id').val(result);
                }
            }
        });
    },



    copyButton: function (user_id) {

        if (!confirm("Are you sure ?")) {
            return false
        }
        var id = $('#open_bot').val();

        console.log(id);
        $.ajax({
            cache: false,
            url: 'bot/'+ id + '/' + id + '.json',
            data: '',
            success: function( jsondata ) {
                console.log(jsondata);

                $.ajax({
                    url: '/time.php',
                    type: this.ajaxMethod,
                    data: '',
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function(new_id){
                        $('#name_id_edit').val(new_id);
                        $('#exchange_edit').val(jsondata.exchange);
                        $('#account_edit').val(jsondata.account);
                        $('#pair_edit').val(jsondata.pair);

                        $('#invest_base_edit').removeAttr('readonly');
                        $('#invest_quote_edit').removeAttr('readonly');
                        $('#add_invest_quote_edit').parent().hide();
                        $('#add_invest_base_edit').parent().hide();
                        $('#hide_for_copy').html('<button type="submit" class="btn btn-info" onclick="set.createCopyButton(' + user_id + ')">Send</button>');

                        var formData = new FormData();

                        formData.append('id', new_id);
                        formData.append('exchange', jsondata.exchange);
                        formData.append('account', jsondata.account);
                        formData.append('pair', jsondata.pair);
                        formData.append('earn', jsondata.earn);

                        $.ajax({
                            url: 'next.php',
                            type: 'POST',
                            data: formData,
                            cache: false,
                            processData: false,
                            contentType: false,
                            success: function(result){
                                // console.log(result);
                            }
                        });

                    }
                });
            }
        });
    },

    createCopyButton: function (user) {
        var formData = new FormData();

        formData.append('id', $('#name_id_edit').val());
        formData.append('exchange', $('#exchange_edit').val());
        formData.append('account', $('#account_edit').val());
        formData.append('pair', $('#pair_edit').val());
        formData.append('earn', $('#earn_edit').val());

        formData.append('investBase', $('#invest_base_edit').val());
        formData.append('investQuote', $('#invest_quote_edit').val());
        formData.append('type', $('#type_edit').val());
        formData.append('maxRange', $('#maxRange_edit').val());
        formData.append('minRange', $('#minRange_edit').val());
        formData.append('takerFee', $('#takerFee_edit').val());
        formData.append('makerFee', $('#makerFee_edit').val());
        formData.append('BaseMinLot', $('#BaseMinLot_edit').val());
        formData.append('QuoteMinLot', $('#QuoteMinLot_edit').val());
        formData.append('minLot', $('#minLot_edit').val());
        formData.append('lambda', $('#lambda_edit').val());
        formData.append('spread', $('#spread_edit').val());
        formData.append('minRRTrailingStop', $('#minRRTrailingStop_edit').val());
        formData.append('distanceTrailingStop', $('#distanceTrailingStop_edit').val());
        formData.append('forceMajor', $('#forceMajor_edit').val());
        formData.append('minRRAlgorithm', $('#minRRAlgorithm_edit').val());
        formData.append('distanceAlgorithm', $('#distanceAlgorithm_edit').val());

        $.ajax({
            url: 'result.php',
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $('#waiting').css('display', 'block');
            },
            success: function(result){
                console.log(result);
            }
        });
        //
        formData.append('user', user);

        $.ajax({
            url: '/data/entry.php',
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function(result){
                console.log(result);
            }
        });
        setTimeout(function(){
            // location.reload();
        }, 2500);

    },

    actionEdit: function (id) {
        $('#open_bot').val(id);

        $('#name_id_edit').val('');
        $('#exchange_edit').val('');
        $('#account_edit').val('');
        $('#pair_edit').val('');
        $('#earn_edit').val('');

        $('#invest_base_edit').val('');
        $('#invest_quote_edit').val('');
        $('#type_edit').val('');
        $('#maxRange_edit').val('');
        $('#minRange_edit').val('');
        $('#takerFee_edit').val('');
        $('#makerFee_edit').val('');
        $('#BaseMinLot_edit').val('');
        $('#QuoteMinLot_edit').val('');
        $('#minLot_edit').val('');
        $('#lambda_edit').val('');
        $('#spread_edit').val('');
        $('#minRRTrailingStop_edit').val('');
        $('#distanceTrailingStop_edit').val('');
        // $('#forceMajor_edit').val('');
        $("#forceMajor_edit").val("0");
        $('#minRRAlgorithm_edit').val('');
        $('#distanceAlgorithm_edit').val('');
        $.ajax({
            cache: false,
            url: '/new/bot/'+ id + '/' + id + '.json',
            data: '',
            success: function( result ) {

                // console.log(result);
                $('#name_id_edit').val(result.id);
                $('#exchange_edit').val(result.exchange);
                $('#account_edit').val(result.account);
                $('#pair_edit').val(result.pair);
                $('#earn_edit').val(result.earn);
                $('#invest_base_edit').val(result.investBase);
                $('#invest_quote_edit').val(result.investQuote);
                $('#type_edit').val(result.type);
                $('#maxRange_edit').val(result.maxRange);
                $('#minRange_edit').val(result.minRange);
                $('#takerFee_edit').val(result.takerFee);
                $('#makerFee_edit').val(result.makerFee);
                $('#BaseMinLot_edit').val(result.BaseMinLot);
                $('#QuoteMinLot_edit').val(result.QuoteMinLot);
                $('#minLot_edit').val(result.minLot);
                $('#lambda_edit').val(result.lambda);
                $('#spread_edit').val(result.spread);
                $('#minRRTrailingStop_edit').val(result.minRRTrailingStop);
                $('#distanceTrailingStop_edit').val(result.distanceTrailingStop);
                $('#forceMajor_edit').val(result.forceMajor);
                $('#minRRAlgorithm_edit').val(result.minRRAlgorithm);
                $('#distanceAlgorithm_edit').val(result.distanceAlgorithm);

            }
        });
    },


    stopButton: function (element) {

        if (!confirm("Are you sure ?")) {
            return false
        }
        var id = $('#open_bot').val();
        var formData = new FormData();
        formData.append('id', id);
        $.ajax({
            url: 'stop.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('#waiting').css('display', 'block');
            },
            success: function(result){
                if(result == 'success'){
                    location.reload();
                }

            }
        });
    },

    
    stopButton_: function (id, element) {

        if (!confirm("Are you sure ?")) {
            return false
        }
        console.log(element);
        $(element).fadeOut();

        var formData = new FormData();
        formData.append('id', id);
        $.ajax({
            url: 'stop.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('#waiting').css('display', 'block');
            },
            success: function(result){
                if(result == 'success'){
                    location.reload();
                }

            }
        });
    },

    saveButton: function () {
        var formData = new FormData();
        var id = $('#open_bot').val();

        var name_id = $('#name_id_edit').val();
        var exchange = $('#exchange_edit').val();
        var account = $('#account_edit').val();
        var pair = $('#pair_edit').val();
        var earn = $('#earn_edit').val();
        var investBase = $('#invest_base_edit').val();
        var investQuote = $('#invest_quote_edit').val();
        var add_invest_base = $('#add_invest_base_edit').val();
        var add_invest_quote = $('#add_invest_quote_edit').val();
        var type = $('#type_edit').val();
        var maxRange = $('#maxRange_edit').val();
        var minRange = $('#minRange_edit').val();
        var takerFee = $('#takerFee_edit').val();
        var makerFee = $('#makerFee_edit').val();
        var BaseMinLot = $('#BaseMinLot_edit').val();
        var QuoteMinLot = $('#QuoteMinLot_edit').val();
        var minLot = $('#minLot_edit').val();
        var lambda = $('#lambda_edit').val();
        var spread = $('#spread_edit').val();
        var minRRTrailingStop = $('#minRRTrailingStop_edit').val();
        var distanceTrailingStop = $('#distanceTrailingStop_edit').val();
        var forceMajor = $('#forceMajor_edit').val();
        var minRRAlgorithm = $('#minRRAlgorithm_edit').val();
        var distanceAlgorithm = $('#distanceAlgorithm_edit').val();


        $.ajax({
            cache: false,
            url: '/new/bot/'+ name_id + '/' + name_id + '.json',
            data: '',
            success: function( result ) {

                var name_id_edit = result.id;
                var exchange_edit = result.exchange;
                var account_edit = result.account;
                var pair_edit = result.pair;

                var earn_edit = result.earn;
                var investBase_edit = result.investBase;
                var investQuote_edit = result.investQuote;
                var type_edit = result.type;
                var maxRange_edit = result.maxRange;
                var minRange_edit = result.minRange;
                var takerFee_edit = result.takerFee;
                var makerFee_edit = result.makerFee;
                var BaseMinLot_edit = result.BaseMinLot;
                var QuoteMinLot_edit = result.QuoteMinLot;
                var minLot_edit = result.minLot;
                var lambda_edit = result.lambda;
                var spread_edit = result.spread;
                var minRRTrailingStop_edit = result.minRRTrailingStop;
                var distanceTrailingStop_edit = result.distanceTrailingStop;
                var forceMajor_edit = result.forceMajor;
                var minRRAlgorithm_edit = result.minRRAlgorithm;
                var distanceAlgorithm_edit = result.distanceAlgorithm;


                if (takerFee_edit != takerFee || makerFee_edit != makerFee) {
                    // UserFees.csv
                    console.log('UserFees.csv');
                    formData.append('user_fees', true);


                }

                if (add_invest_base != '' || add_invest_quote != '') {
                    // AddAsset.csv
                    console.log('AddAsset.csv');
                    formData.append('add_asset', true);

                }


                if (earn_edit != earn ||
                    investBase_edit != investBase ||
                    investQuote_edit != investQuote ||
                    type_edit != type ||
                    maxRange_edit != maxRange ||
                    minRange_edit != minRange ||
                    BaseMinLot_edit != BaseMinLot ||
                    QuoteMinLot_edit != QuoteMinLot ||
                    minLot_edit != minLot ||
                    lambda_edit != lambda ||
                    spread_edit != spread ||
                    forceMajor_edit != forceMajor ||
                    minRRAlgorithm_edit != minRRAlgorithm ||
                    distanceAlgorithm_edit != distanceAlgorithm) {
                    // AlgoParams.csv
                    console.log('AlgoParams.csv');
                    formData.append('algo_params', true);

                }



                if (minRRTrailingStop_edit != minRRTrailingStop ||
                    distanceTrailingStop_edit != distanceTrailingStop) {
                    // AddAsset.csv
                    console.log('TSParams.csv');
                    formData.append('TS_Params', true);

                }


                //
                formData.append('id', name_id);
                formData.append('exchange', exchange);
                formData.append('account', account);
                formData.append('pair', pair);

                formData.append('earn', earn);
                formData.append('investBase', investBase);
                formData.append('investQuote', investQuote);
                formData.append('add_invest_base', add_invest_base);
                formData.append('add_invest_quote', add_invest_quote);
                formData.append('type', type);
                formData.append('maxRange', maxRange);
                formData.append('minRange', minRange);
                formData.append('takerFee', takerFee);
                formData.append('makerFee', makerFee);
                formData.append('BaseMinLot', BaseMinLot);
                formData.append('QuoteMinLot', QuoteMinLot);
                formData.append('minLot', minLot);
                formData.append('lambda', lambda);
                formData.append('spread', spread);
                formData.append('minRRTrailingStop', minRRTrailingStop);
                formData.append('distanceTrailingStop', distanceTrailingStop);
                formData.append('forceMajor', forceMajor);
                formData.append('minRRAlgorithm', minRRAlgorithm);
                formData.append('distanceAlgorithm', distanceAlgorithm);

                $.ajax({
                    url: 'edit.php',
                    type: 'POST',
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        $('#waiting').css('display', 'block');
                    },
                    success: function(result){
                        console.log(result);
                        if(result == 'success'){
                            // window.location.href = '/'
                            location.reload();
                        }
                    }
                });

                var user = $('#user_id').val();
                formData.append('user', user);
                formData.append('bot_type', 'new');

                $.ajax({
                    url: '/data/update.php',
                    type: 'POST',
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function(result){
                        console.log(result);
                        // location.reload();
                    }
                });

            }
        });


    },


    actionRevChange: function (event) {

        var val = $('#revInput_'+event).val();
        $('.isRev-'+event)
            .text(val)
            .addClass("isRevaluatedPrice-"+event)
            .parent().prop("contenteditable", true)
        // console.log(val);

    },


    actionPause: function (id, element) {
        var formData = new FormData();
        formData.append('id', id);
        var text = $(element).text();
        var text = $.trim(text);
        // console.log(text);
        if(text == "Pause"){
            formData.append('action', 'pause');
            $(element).text( text == "Activate" ? "Pause" : "Activate");
        }else{
            formData.append('action', 'activate');
            $(element).text( text == "Pause" ? "Activate" : "Pause");
        }
        $.ajax({
            url: 'pause.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){

            },
            success: function(result){
                // console.log(result);
                if(result == 'success'){
                    $(element).toggleClass('btn-info btn-success');
                }
            }
        });
    },

    actionPos: function (event) {
        // if (!confirm("Are you sure ?")) {
        //     return false
        // }

        var bot = $('#open_bot').val();
        var formData = new FormData();
        formData.append('id', bot);
        formData.append('event', event);

        var element = $('.form-check-input-'+event);
        var switch_ = $('.switch-check-input-'+event);

        for (var k=0;k<switch_.length;k++){
            var line_switch = switch_[k];
            if ($(switch_[k]).is(':checked')) {
                formData.append('switch[\'' + $(line_switch).data("id") + '\']', '1');
            }else if($(line_switch).parent().parent().parent().find('.isChecked').length > 0){
//                 console.log($(line_switch).parent().parent().parent().find('.isChecked').length);
                formData.append('switch[\'' + $(line_switch).data("id") + '\']', '0');
            }
        }
        for (var i=0;i<element.length;i++){
            var line = element[i];
            if ($(element[i]).is(':checked')) {
                formData.append('data-id[\''+$(line).data("id")+'\']', '1');
            }


            if($(line).parent().parent().parent().find('.isRevaluatedPrice-'+event)){
                let rev = $(line).parent().parent().parent().find('.isRevaluatedPrice-'+event).text();
                // console.log(rev);
                if(rev != ''){
                    formData.append('revaluatedPrice[\'' + $(line).data("id") + '\']', rev);
                }
            }

        }
        $.ajax({
            url: 'close_pos.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('#waiting').css('display', 'block');
            },
            success: function(result){
                $('#waiting').css('display', 'none');
                console.log(result)
                if(result == 'success'){
                    location.reload();
                }else if(result == 'in_process'){
                    alert(result);
                }
            }
        });
    },

    downloadButton: function (file) {
        var id = $('#open_bot').val();

        // console.log(file);
        window.location.href = 'download.php?id='+id+'&file='+file;
    },

};

$("#next").click(function(){

    var name_id = $('#name_id').val();
    var exchange = $('#exchange').val();
    var account = $('#account').val();
    var pair = $('#pair').val();
    var earn = $('#earn').val();

    $('#step_1').hide();
    $('#step_2').show();
    $('#modal_footer').show();

    var formData = new FormData();

    formData.append('id', name_id);
    formData.append('exchange', exchange);
    formData.append('account', account);
    formData.append('pair', pair);
    formData.append('earn', earn);

    $.ajax({
        url: 'next.php',
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        success: function(result){
            console.log(result);
        }
    });
});

$("#send").click(function(){
    var formData = new FormData();

    var investBase = $('#invest_base').val();
    var investQuote = $('#invest_quote').val();
    var type = $('#type').val();
    var maxRange = $('#maxRange').val();
    var minRange = $('#minRange').val();
    var takerFee = $('#takerFee').val();
    var makerFee = $('#makerFee').val();
    var BaseMinLot = $('#BaseMinLot').val();
    var QuoteMinLot = $('#QuoteMinLot').val();
    var minLot = $('#minLot').val();
    var lambda = $('#lambda').val();
    var spread = $('#spread').val();
    var minRRTrailingStop = $('#minRRTrailingStop').val();
    var distanceTrailingStop = $('#distanceTrailingStop').val();
    var forceMajor = $('#forceMajor').val();
    var minRRAlgorithm = $('#minRRAlgorithm').val();
    var distanceAlgorithm = $('#distanceAlgorithm').val();

    if(
    investBase == '' ||
    investQuote == '' ||
    type == '' ||
    maxRange == '' ||
    minRange == '' ||
    takerFee == '' ||
    makerFee == '' ||
    BaseMinLot == '' ||
    QuoteMinLot == '' ||
    minLot == '' ||
    lambda == '' ||
    spread == '' ||
    minRRTrailingStop == '' ||
    distanceTrailingStop == '' ||
    minRRAlgorithm == '' ||
    distanceAlgorithm  == ''
    ){
        alert('Missing data');
        // console.log(investBase)
        // console.log(investQuote)
        // console.log(type)
        // console.log(maxRange)
        // console.log(minRange)
        // console.log(takerFee)
        // console.log(makerFee)
        // console.log(BaseMinLot)
        // console.log(QuoteMinLot)
        // console.log(minLot)
        // console.log(lambda)
        // console.log(spread)
        // console.log(minRRTrailingStop)
        // console.log(distanceTrailingStop)
        // console.log(forceMajor)
        // console.log(minRRAlgorithm)
        // console.log(distanceAlgorithm)
        return false

    }


    formData.append('id', $('#name_id').val());
    formData.append('exchange', $('#exchange').val());
    formData.append('account', $('#account').val());
    formData.append('pair', $('#pair').val());
    formData.append('earn', $('#earn').val());

    formData.append('investBase', $('#invest_base').val());
    formData.append('investQuote', $('#invest_quote').val());
    formData.append('type', $('#type').val());
    formData.append('maxRange', $('#maxRange').val());
    formData.append('minRange', $('#minRange').val());
    formData.append('takerFee', $('#takerFee').val());
    formData.append('makerFee', $('#makerFee').val());
    formData.append('BaseMinLot', $('#BaseMinLot').val());
    formData.append('QuoteMinLot', $('#QuoteMinLot').val());
    formData.append('minLot', $('#minLot').val());
    formData.append('lambda', $('#lambda').val());
    formData.append('spread', $('#spread').val());
    formData.append('minRRTrailingStop', $('#minRRTrailingStop').val());
    formData.append('distanceTrailingStop', $('#distanceTrailingStop').val());
    formData.append('forceMajor', $('#forceMajor').val());
    formData.append('minRRAlgorithm', $('#minRRAlgorithm').val());
    formData.append('distanceAlgorithm', $('#distanceAlgorithm').val());

    $.ajax({
        url: 'result.php',
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {
            $('#waiting').css('display', 'block');
        },
        success: function(result){
            console.log(result);
        }
    });
    var user_id = $('#user_id').val();
    formData.append('user', user_id);
    $.ajax({
        url: '/data/entry.php',
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        success: function(result){
            console.log(result);
        }
    });
    setTimeout(function(){
        location.reload();
    }, 2500);


});



function reRun(account, id, exchange, pair, type) {

    var formData = new FormData();
    formData.append('id', id);
    formData.append('exchange', exchange);
    formData.append('account', account);
    formData.append('pair', pair);
    formData.append('type', type);
    $.ajax({
        url: 'rerun.php',
        type: "POST",
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        success: function (result) {
            console.log(result);
        }
    });
    setTimeout(function(){
        location.reload();
    }, 2500);

}