<?php
if(isset($_POST['id'])) {

    if (!file_exists('bot/' . $_POST['id'])) {
        mkdir('bot/' . $_POST['id'], 0777, true);
    }

    $arr = $_POST;
    $str = "{\n";
    foreach ($arr as $key => $val) {
        $str .= '"' . $key . '"' . ':' . '"' . $val . '"' . ",\n";
    }

    $str = mb_substr($str, 0, -2);

    file_put_contents('bot/' . $_POST['id'] . "/" . $_POST['id'] . '.json', $str . "\n}");


    $takerFee = $arr['takerFee'];
    $makerFee = $arr['makerFee'];
    $str_set = "$takerFee,$makerFee";
    file_put_contents('bot/' . $_POST['id'] . "/UserFees.csv", $str_set);
    file_put_contents('bot/' . $_POST['id'] . "/" . 'redUserFees', date("Y-m-d H-i-s") . "\n", FILE_APPEND);


    $investBase = $arr['investBase'];
    $investQuote = $arr['investQuote'];
    $str_set = "$investBase,$investQuote";
    file_put_contents('bot/' . $_POST['id'] . "/AddAsset.csv", $str_set);
    file_put_contents('bot/' . $_POST['id'] . "/" . 'redAddAsset', date("Y-m-d H-i-s") . "\n", FILE_APPEND);


    $type = $arr['type'] . $arr['earn'];
    $minRange = $arr['minRange'];
    $maxRange = $arr['maxRange'];
    $lambda = $arr['lambda'];
    $QUOTEMinLot = $arr['QuoteMinLot'];
    $BASEMinLot = $arr['BaseMinLot'];
    $minLot = $arr['minLot'];
    $spread = $arr['spread'];
    $minRRAlgorithm = $arr['minRRAlgorithm'];
    $distanceAlgorithm = $arr['distanceAlgorithm'];
    $forceMajor = $arr['forceMajor'];
    $str_set = "$type,$minRange,$maxRange,$lambda,$QUOTEMinLot,$BASEMinLot,$minLot,$spread,$minRRAlgorithm,$distanceAlgorithm,$forceMajor";
    file_put_contents('bot/' . $_POST['id'] . "/AlgoParams.csv", $str_set);
    file_put_contents('bot/' . $_POST['id'] . "/" . 'redAlgoParams', date("Y-m-d H-i-s") . "\n", FILE_APPEND);


    $minRRTrailingStop = $arr['minRRTrailingStop'];
    $distanceTrailingStop = $arr['distanceTrailingStop'];

    $str_set = "$minRRTrailingStop,$distanceTrailingStop";
    file_put_contents('bot/' . $_POST['id'] . "/TSParams.csv", $str_set);
    file_put_contents('bot/' . $_POST['id'] . "/" . 'redTSParams', date("Y-m-d H-i-s") . "\n", FILE_APPEND);

    chmod('bot/' . $_POST['id'] . "/" . 'redTSParams', 0777);
    chmod('bot/' . $_POST['id'] . "/" . 'redAlgoParams', 0777);
    chmod('bot/' . $_POST['id'] . "/" . 'redAddAsset', 0777);
    chmod('bot/' . $_POST['id'] . "/" . 'redUserFees', 0777);
    chmod('bot/' . $_POST['id'] . "/" . 'TSParams.csv', 0777);
    chmod('bot/' . $_POST['id'] . "/" . 'AlgoParams.csv', 0777);
    chmod('bot/' . $_POST['id'] . "/" . 'AddAsset.csv', 0777);
    chmod('bot/' . $_POST['id'] . "/" . 'UserFees.csv', 0777);

    unlink('bot/' . $_POST['id'] . "/" . 'pauseA');

//header('Location: /');
    $dir = ($_SERVER['DOCUMENT_ROOT']);
    $bot_dir = $dir . "/new/bot/" . $_POST['id'] . "/";
    $key_dir = $dir . "/base/" . $_POST["exchange"] . '/' . $_POST['account'] . "/key.json";
    $exchange = $_POST["exchange"];
    $pair = $_POST["pair"];

    $command1 = escapeshellcmd("/usr/bin/python  /home/talisant/Surfer_final/mainAlgorithm1.py ${bot_dir} ${exchange} ${pair} >& ${bot_dir}/mainalgo.log  & ");
    while(!file_exists("$bot_dir/stopA")) {
        exec($command1);
    }
}
