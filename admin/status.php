<?php include './header.php';
//ini_set("display_errors",1);
//error_reporting(E_ALL);
require "all.php";
require "all_get.php";



?>
<div class="container" style="min-width: 100em !important;">
    <div class="row">
        <table  id="bots" style="text-align: left">
            <tr>
                <th>
<!--                    <form>-->
                        <div class="form-group">
                            <input type="text" id="datepicker_start" class="form-control filter-input" placeholder="Begin" value="<?=$time_start;?>">
                            <input type="text" id="datepicker_end" class="form-control filter-input" placeholder="End" value="<?=$time_end;?>">
                        </div>
<!--                    </form>-->
                </th>
                <th style="text-align: right">
                    <select name="acc_option" id="acc_option" class="acc_options" onchange="set.ChangeAccOptionAd()">
                        <option value="">Account</option>
                        <?=$options_allow;?>
                    </select>
                </th>
                <th style="text-align: right">
                    <select name="pair_option" id="pair_option" class="acc_options" onchange="set.ChangePairOption()" style="width: 60px">
                        <option value="">Pair</option>
                        <?=$options_pair;?>
                    </select>
                </th>
<!--                <th>Pair</th>-->
                <?php
                if(isset($_GET['new']) || isset($_GET['test'])){
                    echo "<th>Invest B | Q</th></td>";
                }else{
                    echo "<th>Investment</th>";
                }

                ?>

                <th>
                    <select name="active_option" id="active_option" class="acc_options" onchange="set.ChangeActOption()" style="width: 40px">
                        <option value="">select..</option>
                        <option value="active">Active</option>
                        <option value="paused">Paused</option>
                    </select>
                </th>
                <th>

                    <select name="remove_option" id="remove_option" class="acc_options" onchange="set.ChangeRemOption()" style="width: 50px">
                        <option value="">select..</option>
                        <option value="removed">Removed</option>
                        <option value="exist">Exist</option>
                    </select>
                </th>
                <th>Stop</th>
                <th>Status</th>
                <th>Action</th>
            </tr>

            <?php



            $comand_all =  shell_exec("ps -ef | grep talisant/$command_folder" );
            $comand_all_arr = explode("\n", $comand_all);

            $comand_all_tick =  shell_exec("ps -ef | grep talisant/tickers/" );
            $comand_all_arr_tickers = explode("\n", $comand_all_tick);

            $comand_all_arr_edit = array();
            $comand_all_arr_order = array();



            foreach($comand_all_arr as $item){

                $pos_edit = strpos($item, $pos_edit_str);
                $pos_order = strpos($item, $pos_order_str);

                if($pos_edit != false){
                    $comand_all_arr_edit[] = $item;
                }
                if($pos_order != false){
                    $comand_all_arr_order[] = $item;
                }

            }

            foreach ($bot_folders as $bot_folder) {
                if(strlen($bot_folder) < 6){
                    unset($bot_folder, $bot_folders);
                    continue;
                }
                $dir = $_SERVER['DOCUMENT_ROOT'] . "$bot_dir_name/$bot_folder";
                $files_ = scandir($dir);
                $id = $bot_folder;

                $removed = 'exist';
                if (in_array("remove", $files_)) {
                    $removed = 'removed';
                }

                $active = 'active';
                if (in_array("pause", $files_) || in_array("pauseA", $files_)) {
                    $active = 'paused';
                }
                $stop = 'active';
                if (in_array("stop", $files_) || in_array("stopA", $files_) || in_array("stopM", $files_)) {
                    $stop = 'stop';
                }

                $json = file_get_contents("..$bot_dir_name/$bot_folder/$bot_folder.json");
                $data = (json_decode($json));
                $pair = $data->pair;
                $exchange = $data->exchange;
                $account = $data->account;

                if (isset($_GET['filter']) && $_GET['filter'] != '' && $_GET['filter'] != $account) {
                    continue;
                } else {
                    if ((isset($_GET['start']) && $_GET['start'] > $bot_folder) || (isset($_GET['end']) && $_GET['end']<$bot_folder)){
                        continue;
                    } else {
                        if (isset($_GET['pair']) && $_GET['pair'] != '' && $_GET['pair'] != $pair) {
                            continue;
                        } else {
                            if (isset($_GET['active']) && $_GET['active'] != '' && $_GET['active'] != $active) {
                                continue;
                            } else {
                                if (isset($_GET['remove']) && $_GET['remove'] != '' && $_GET['remove'] != $removed) {
                                    continue;
                                } else {
                                    ?>
                                    <tr>
                                        <td onclick="set.actionOpen('<?= $bot_folder; ?>')"><a
                                                    href="javascript: void(0)"><?= $bot_folder; ?>
                                                (<?= date("m-d H:i:s", ($bot_folder + 4 * 3600)); ?>)</a></td>
                                        <td><?= $account; ?></td>
                                        <td><?= $data->pair; ?></td>
                                        <?php
                                        if(isset($_GET['new']) || isset($_GET['test'])){
                                            echo "<td>$data->investBase | $data->investQuote</td>";

                                        }else{
                                            echo "<td>$data->investment</td>";
                                        }

                                        ?>

                                        <td><?= $active; ?></td>
                                        <td><?= $removed; ?></td>
                                        <td><?= $stop; ?></td>
                                        <td>
                                            <?php

//                                            if ($removed == 'exist') {


                                                $status_color = 'red';
                                                $ticker_turnON = "onclick=\"turnOnTicker('$pair', '$exchange')\" style='cursor: pointer'";
                                                foreach ($comand_all_arr_tickers as $item) {
                                                    $pos_tick = strpos($item, $exchange . ' ' . $pair);
                                                    if ($pos_tick != false) {
                                                        $status_color = 'green';
                                                        $ticker_turnON = '';
                                                        break;
                                                    }

                                                }

                                                echo "<span class='fa fa-power-off $status_color-icon' data-toggle='tooltip' data-placement='top' title='Ticker' $ticker_turnON></span> ";
//

                                                $status_color = 'red';
                                                foreach ($comand_all_arr_edit as $item) {
                                                    $pos_edit = strpos($item, $bot_folder);
                                                    if ($pos_edit != false) {
                                                        $status_color = 'green';
                                                        break;
                                                    }
                                                }
                                                echo "<span class='fa fa-power-off $status_color-icon' data-toggle='tooltip' data-placement='top' title='$tirle_1'></span> ";


                                                $status_color = 'red';
                                            $order_turnON = "onclick=\"reRun('$data->account', '$bot_folder', '$data->exchange')\" style='cursor: pointer'";

                                            foreach ($comand_all_arr_order as $item) {
                                                    $pos_order = strpos($item, $bot_folder);
                                                    if ($pos_order != false) {
                                                        $status_color = 'green';
                                                        $order_turnON = '';
                                                        break;
                                                    }
                                                }
                                                echo "<span class='fa fa-power-off $status_color-icon' data-toggle='tooltip' data-placement='top' title='$tirle_2' $order_turnON></span> ";


//                                            }

                                            ?>
                                        </td>

                                        <td>
                                            <form method="post" style="text-align: center">
                                                <input type="hidden" name="show" value="<?= $bot_folder; ?>">
                                                <input type="submit" value="Show Process" class="btn btn-info">
                                            </form>
                                            <?php
                                            if(isset($_GET['new']) || isset($_GET['test'])){
                                                ?>

                                                <form method="post" style="text-align: center; margin-top: 10px">
                                                    <input type="hidden" name="delete_new_algo" value="<?= $bot_folder; ?>">
                                                    <input type="submit" value="Stop Algo" class="btn btn-danger">
                                                </form>
                                                <form method="post" style="text-align: center; margin-top: 10px">
                                                    <input type="hidden" name="delete_new_market" value="<?= $bot_folder; ?>">
                                                    <input type="submit" value="Stop Market" class="btn btn-danger">
                                                </form>
                                                <?php
                                            }else {
                                                ?>
                                                <form method="post" style="text-align: center; margin-top: 10px">
                                                    <input type="hidden" name="delete" value="<?= $bot_folder; ?>">
                                                    <input type="submit" value="Stop Process" class="btn btn-danger">
                                                </form>
                                                <?php
                                            }
                                                ?>
                                        </td>
                                    </tr>

                                <?php }
                            }
                        }
                    }
                }
            }
            ?>

        </table>
    </div>
</div>
<button type="button" class="hidden" id="button_open" data-toggle="modal" data-target="#myModalOpen"></button>
<div class="modal fade" id="myModalOpen" role="dialog" style="text-align: left;">
    <div class="modal-dialog">

        <!-- Modal content-->
        <!-- OPEN MODAL-->
        <div class="modal-content" style="display: table-row-group">
            <div class="modal-header" style="border: none">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="open_modal_header"></h4>
                <div id="ticker_result"></div>
                <div id="balance_result"></div>

            </div>

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#menu1">userSchedule</a></li>
                <li><a data-toggle="tab" href="#menu2">orderHistory</a></li>
                <li><a data-toggle="tab" href="#menu4">Balance</a></li>
                <li><a data-toggle="tab" href="#menu3">newOrderInfo</a></li>
            </ul>

            <div class="tab-content">

                <div id="menu1" class="tab-pane fade in active">
                    <div class="modal-body" style="padding-left: 30px;">
                        <table class="bots" id="schedule">
                            <div id="open_tbody_checkbox">
                            </div>
                            <tr>
                                <th>Level</th>
                                <th>Amount</th>
                                <th>Price</th>
                                <th>Total_Value</th>
                                <th>Profit</th>
                                <th>Effective_Price</th>
                                <th>RL</th>
                            </tr>
                            <tbody id="open_tbody">
                            </tbody>
                        </table>
                        <input value="Export as CSV Schedule" style="margin-top: 15px;" type="button" class="btn btn-info" onclick="$('#schedule').table2CSV({header:['Level','Amount','Price','Total_Value','Profit','Effective_Price','RL']}, 'schedule')">
                    </div>
                </div>


                <div id="menu2" class="tab-pane fade">
                    <div class="modal-body" style="padding-left: 30px;">
                        <table class="bots">
                            <tbody id="open_tbody_history">
                            </tbody>
                        </table>
                        <a type="submit" class="btn btn-info" style="margin-top: 15px;"  onclick="set.downloadButton('orderHistory')">Export as CSV History</a>
                    </div>
                </div>


                <div id="menu3" class="tab-pane fade">
                    <div class="modal-body" style="padding-left: 30px;">
                        <div id="newOrderInfo_result"></div>
                    </div>
                </div>


                <div id="menu4" class="tab-pane fade">
                    <div class="modal-body" style="padding-left: 30px;">
                        <table class="bots" id="state_summary">
                            <tr>
                                <th>Y</th>
                                <th>X</th>
                                <th>Left Investment</th>
                                <th>Initial Investment</th>
                                <th>Total Investment in Current Price</th>
                                <th>Profit in Current Price</th>
                                <th>Efficiency %</th>
                            </tr>
                            <tbody id="open_tbody_stateSummary">
                            </tbody>
                        </table>
                        <input value="Export as CSV Balance" style="margin-top: 15px;" type="button" class="btn btn-info" onclick="$('#state_summary').table2CSV({header:['Y', 'X', 'Left Investment', 'Initial Investment', 'Total Investment in Current Price', 'Profit in Current Price', 'Efficiency %']}, 'Balance')">
                    </div>
                </div>


            </div>

            <input type="hidden" id="open_bot">
            <div class="modal-footer">
                <!--                    <a type="submit" class="btn btn-info" style="float:left;"  onclick="set.downloadButton('userSchedule')">Download Schedule</a>-->
<!--                <button class="btn btn-danger" id="close_position" onclick="set.lineClose(this)">close positions</button>-->
                <button type="button" id="close_modal" class="btn btn-secondary" data-dismiss="modal">Cancel
                </button>

            </div>
        </div>
        <!--END OPEN MODAL-->

    </div>
</div>

<!--==========================-->

<!--++++++++++++++-->
<div class="container">
    <div class="row">

    </div>
</div>

<script type="text/javascript">
    $(function() {
        $( "#datepicker_start" ).datepicker({
            dateFormat : 'yy-mm-dd',
            // dateFormat : 'dd/mm/yy',
            changeMonth : true,
            changeYear : true,
            yearRange: '-100y:c+nn',
            maxDate: '-0d'
        });
        $( "#datepicker_end" ).datepicker({
            dateFormat : 'yy-mm-dd',
            changeMonth : true,
            changeYear : true,
            yearRange: '-100y:c+nn',
            maxDate: '-0d'
        });
    });
    $("#datepicker_start").change(function(){
        var date_start = $("#datepicker_start").val();
        console.log(date_start);
        var myDate=date_start;
        myDate=myDate.split("-");
        var newDate=myDate[1]+"/"+myDate[2]+"/"+myDate[0];
        var time = (new Date(newDate).getTime());
        time = time/1000;
        time = time+(4*3600);
        console.log(time);
        var url = (window.location.href);
        var n = url.indexOf('?');
        var n_ = url.indexOf('NaN');
        if(n_ >= 0) {
            window.location.href = '/admin/status.php';
            return false;
        };
        if(n >= 0){
            window.location.href = url + '&start=' + time;
        }else{
            window.location.href = url + '?start=' + time;
        }

    });
    $("#datepicker_end").change(function(){
        var date_start = $("#datepicker_end").val();
        console.log(date_start);
        var myDate=date_start;
        myDate=myDate.split("-");
        var newDate=myDate[1]+"/"+myDate[2]+"/"+myDate[0];
        var time = (new Date(newDate).getTime());
        time = time/1000;
        time = time+(4*3600);
        console.log(time);
        var url = (window.location.href);
        var n_ = url.indexOf('NaN');
        if(n_ >= 0) {
            window.location.href = '/admin/status.php';
            return false;
        };
        var n = url.indexOf('?');
        if(n >= 0){
            window.location.href = url + '&end=' + time;
        }else{
            window.location.href = url + '?end=' + time;
        }


    });

</script>
<!--==========================-->

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="../../admin/script.js"></script>
<script src="../daynight.js"></script>

<?php
//include '../footer.php';

?>

