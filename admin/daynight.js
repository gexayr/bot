function day() {
    $('body').removeClass("darkSwitch");
    $('body').addClass("lightSwitch");
    writeCookie('mode', 'day', 'write');
    $('#theme_switch').css("color","#518cc4");
    $('#theme_switch').attr( "onclick", "night()");



};

function night() {
    $('body').removeClass("lightSwitch");
    $('body').addClass("darkSwitch");
    $('#theme_switch').css("color","#fabe00");
    $('#theme_switch').attr( "onclick", "day()");
    writeCookie('mode', 'night', 'write');

};


function writeCookie(name, value, action) {
    var formData = new FormData();
    if(action == 'write'){
        formData.append('cookie_name', name);
        formData.append('cookie_val', value);
    }else if(action == 'remove'){
        formData.append('rem_cookie', name);
    }

    $.ajax({
        url: '/write.php',
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {

        },
        success: function(result){
            console.log(result);
        }
    });
};