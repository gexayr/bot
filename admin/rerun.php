<?php
ini_set("display_errors",1);
error_reporting(E_ALL);
if(isset($_POST['check']) && $_POST['check'] == 'check'){
    $dir = ($_SERVER['DOCUMENT_ROOT']);
    $bot_dir = $dir."/bot/".$_POST['id']."/";

    $files = scandir($bot_dir);
    if (in_array("order", $files)) {
        echo 'isset_order';
    }else{
        echo 'not_order';
    }
    die();
}

if(isset($_POST['id']) && !isset($_POST['step'])){
    $dir = ($_SERVER['DOCUMENT_ROOT']);
    $bot_dir = $dir."/bot/".$_POST['id']."/";

        $files = scandir($bot_dir);
        if (in_array("order", $files)) {
            unlink($bot_dir . "/" . 'order');
        }

    $key_dir = $dir."/base/" . $_POST["exchange"] . '/' . $_POST['account'] . "/key.json";
    $command1=null;
    if($_POST['exchange'] == 'binance'){
        $command1 = escapeshellcmd("/usr/bin/python  /home/talisant/AlgoXY_new/binance_order.py ${bot_dir} ${key_dir} >& ${bot_dir}order.log  & ");
    }elseif($_POST['exchange'] == 'bitfinex'){
        $command1 = escapeshellcmd("/usr/bin/python  /home/talisant/AlgoXY_new/order_new_test.py ${bot_dir} ${key_dir} >& ${bot_dir}order.log  & ");
    }elseif ($_POST['exchange'] == 'df'){
        $command1 = escapeshellcmd("/usr/bin/python  /home/talisant/AlgoXY_new/df_order.py ${bot_dir} ${key_dir} >& ${bot_dir}order.log  & ");
    }

    exec($command1);
}elseif(isset($_POST['id']) && isset($_POST['step'])){

    $json_order = file_get_contents("../bot/" . $_POST['id'] . "/orderStatus.json");
    $data_order = (json_decode($json_order));
    $str_set = "{";
    foreach ($data_order as $key => $val) {
        if($key == "executedAmount"){
            $val = 0;
        }elseif ( $val == '' || is_string($val) ){
            $val = "\"$val\"";
        }
        $str_set .= '"' . $key . '"' . ':' . $val . ", ";
    }
    $str_set = mb_substr($str_set, 0, -2);

    file_put_contents('../bot/' . $_POST['id'] . "/orderStatus.json", $str_set . "}");
    file_put_contents('../bot/' . $_POST['id'] . "/orderStatusReady", date("Y-m-d H-i-s")  . "\n");
//    die($str_set);

    echo 'success';
}
