var set = {
    ajaxMethod: 'POST',

    remUser: function (id, element) {
        if (!confirm("Are you sure ?")) {
            return false
        }
        // console.log(($(element)).parent().parent().parent());
        var tr = (($(element)).parent().parent().parent());

        var formData = new FormData();
        formData.append('id', id);

        $.ajax({
            url: '/admin/accounts/remove.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {

            },
            success: function (result) {
                console.log(result);
                $(tr).hide();

                // if(result == 'success'){
                //     window.location.href = '/admin/accounts'
                // }
            }
        });

    },

    // userSchedule modal

    actionOpenInfo: function (id,type) {
        $('#button_open').click();

        var formData = new FormData();
        formData.append('id', id);
        formData.append('type', type);

        $.ajax({
            url: '/admin/accounts/edit_info.php',
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function( result ) {
                console.log('===============================');
                // console.log(result);
                console.log('===============================');
                $('#open_tbody_all').html(result);

            }
        });
    },

    actionOpen: function (id) {
        console.log('open ' + id);
        $('#button_open').click();
        $('#open_modal_header').text(id);
        $('#edit_stop_button').text(id);
        $('#open_bot').val(id);
        var count = 1;

        var html ='';

        var formData = new FormData();
        formData.append('id', id);
        formData.append('file', 'orderHistory');

        $.ajax({
            url: '../../csv_read.php',
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){

            },
            success: function(result){
                html = result;
                $('#open_tbody_history').html(html);
            }
        });


        show_BalanceTab(id, count);
        function show_BalanceTab(id, count) {
            var html_bb = '';
            formData.append('id', id);
            formData.append('file', 'stateSummary');

            $.ajax({
                url: '../../csv_read.php',
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $('#open_tbody_stateSummary').html('');

                },
                success: function (result) {
                    // console.log(count);
                    // console.log(result);
                    if (result == '') {
                        console.log('balanceTab not load '+ count);
                        count++;
                        if(count < 40){
                            show_BalanceTab(id, count)
                        }
                    }
                    $('#open_tbody_stateSummary').html(result);
                }
            });
        }

        csv_read(count);

        function csv_read() {

            formData.append('file', 'userSchedule');
            $.ajax({
                url: '../../csv_read.php',
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {

                },
                success: function (result) {
                    if (result == '') {
                        console.log('schedule not load '+ count);
                        $('#open_tbody_checkbox').html('');
                        count++
                        if(count < 40){
                            csv_read(count);
                        }
                    }

                    html = result;
                    $('#open_tbody').html(html);
                }
            });
        }


        $.ajax({
            url: '/bot/'+ id + '/newOrderInfo',
            data: '',
            cache: false,
            processData: false,
            contentType: false,
            success: function( result ) {
                // console.log(result);
                $('#newOrderInfo_result').html(result);

            }
        });


    },


    ChangeAccOptionAd: function() {

        var option = ($('#acc_option').val());

        console.log(option);
        // window.location.href = '/admin/status.php/?filter=' + option;
        var url = (window.location.href);
        var n = url.indexOf('?');
        if(n >= 0){
            window.location.href = url + '&filter=' + option;
        }else{
            window.location.href = url + '?filter=' + option;
        }
    },


    ChangePairOption: function() {

        var option = ($('#pair_option').val());

        var url = (window.location.href);
        var n = url.indexOf('?');
        if(n >= 0){
            window.location.href = url + '&pair=' + option;
        }else{
            window.location.href = url + '?pair=' + option;
        }

    },

    ChangeActOption: function() {

        var option = ($('#active_option').val());

        var url = (window.location.href);
        var n = url.indexOf('?');
        if(n >= 0){
            window.location.href = url + '&active=' + option;
        }else{
            window.location.href = url + '?active=' + option;
        }

    },

    ChangeRemOption: function() {

        var option = ($('#remove_option').val());

        var url = (window.location.href);
        var n = url.indexOf('?');
        if(n >= 0){
            window.location.href = url + '&remove=' + option;
        }else{
            window.location.href = url + '?remove=' + option;
        }

    },

    downloadButton: function (file) {
        var id = $('#open_bot').val();
        window.location.href = '/download.php?id='+id+'&file='+file;
    },
};

function turnOnTicker(pair, exchange) {
    var formData = new FormData();
    formData.append('pair', pair);
    formData.append('exchange', exchange);

    $.ajax({
        url: '/admin/tickerOn.php',
        type: "POST",
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {

        },
        success: function (result) {

        }
    });
    setTimeout(function(){
        location.reload();
    }, 2500);


}

function reRun(account, id, exchange) {
    if (!confirm("Are you sure ?")) {
        return false
    }
    var formData = new FormData();
    formData.append('id', id);
    formData.append('check', 'check');

    $.ajax({
        url: '/admin/rerun.php',
        type: "POST",
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        success: function (result) {
            console.log(result);
            console.log(1);

            if(result == 'not_order') {
                formData.append('check', 'checked');

                formData.append('exchange', exchange);
                formData.append('account', account);

                $.ajax({
                    url: '/admin/rerun.php',
                    type: "POST",
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        console.log(result);
                    }
                });

                if (exchange == 'df'){

                    formData.append('step', '2');

                    $.ajax({
                        url: '/admin/rerun.php',
                        type: "POST",
                        data: formData,
                        cache: false,
                        processData: false,
                        contentType: false,
                        success: function (result) {
                            console.log(result);
                            if (result == 'success') {
                                // window.location.href = '/admin/status.php'
                                location.reload();
                            }
                        }

                    });
                }

            }else if(result == 'isset_order'){
                formData.append('check', 'checked');
                formData.append('exchange', exchange);
                formData.append('account', account);

                $.ajax({
                    url: '/admin/rerun.php',
                    type: "POST",
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        console.log(result);
                    }
                });


                formData.append('step', '2');

                $.ajax({
                    url: '/admin/rerun.php',
                    type: "POST",
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        console.log(result);
                        if(result == 'success'){
                            // window.location.href = '/admin/status.php'
                            location.reload();
                        }
                    }

                });

            }

        }
    });

}