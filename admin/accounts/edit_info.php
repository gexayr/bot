<?php
if(isset($_POST['id'])) {
    require "../../pdo/config.php";
    try {
        $connection = new PDO($dsn, $username, $password, $options);
        $id = $_POST['id'];

        $sql = "SELECT * FROM edit WHERE bot_id = :id";
        $statement = $connection->prepare($sql);
        $statement->bindValue(':id', $id);
        $statement->execute();

        $edit = $statement->fetchAll();

    } catch (PDOException $error) {
        echo '<div class="alert alert-danger alert-dismissible fade in col-sm-6 col-sm-offset-3" style="text-align: center">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Error!</strong> ' . $error->getMessage() . '
                </div>';
    }
    ?>
    <table id="bots" style="text-align: left">
        <thead>
        <tr>
            <td>Trader</td>
<!--            <td>Program mod</td>-->
            <td>Id</td>
            <td>Exchange</td>
            <td>Account</td>
            <td>Pair</td>
            <td>Earn</td>
            <td>Date</td>
            <?php
                if($_POST['type'] == 'Old'){
            ?>
            <td>Investment</td>
            <td>Upper Threshold</td>
            <td>Lower Threshold</td>
            <td>Levels</td>
            <td>X min lot</td>
            <td>Y min lot</td>
            <td>Fee</td>
            <td>Spread</td>
            <td>Lambda</td>
            <td>RR</td>
            <td>Y</td>
            <td>X</td>
            <td>Left Investment</td>

                <?php }else{ ?>
                    <td>Invest_BASE</td>
                    <td>Invest_QUOTE</td>
                    <td>add_BASE</td>
                    <td>add_QUOTE</td>
                    <td>Type</td>
                    <td>max_Range</td>
                    <td>min_Range</td>
                    <td>taker_Fee</td>
                    <td>maker_Fee</td>
                    <td>Base_min_lot</td>
                    <td>Quote_min_lot</td>
                    <td>min_lot</td>
                    <td>lambda</td>
                    <td>spread</td>
                    <td>minRR_TS</td>
                    <td>distance_TS</td>
                    <td>forece_Major</td>
                    <td>minRR_Algo</td>
                    <td>distance_Algo</td>
                <?php } ?>
            <td>Initial Investment</td>
            <td>Total Investment in Current Price</td>
            <td>Profit in Current Price</td>
            <td>Efficiency %</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($edit as $row) : ?>

            <?php
            $bot_data = json_decode($row['bot_data']);

            $timestamp = strtotime($row['date']);
            $timestamp += 4*3600;
            $row['date'] = date('Y-m-d H:i:s', $timestamp);

            $p1 = explode(',', $row['state_summary']);
            ?>
            <tr>
                <td><?=$bot_data->user?></td>
<!--                <td>--><?//=$program_mod;?><!--</td>-->
                <td><?=$id;?></td>
                <td><?=$bot_data->exchange;?></td>
                <td><?=$bot_data->account;?></td>
                <td><?=$bot_data->pair;?></td>
                <td><?=$bot_data->earn;?></td>
                <td><?=$row['date'];?></td>
        <?php
            if($_POST['type'] == 'Old'){
        ?>
                <td><?=$bot_data->investment;?></td>
                <td><?=$bot_data->upper_threshold;?></td>
                <td><?=$bot_data->lower_threshold;?></td>
                <td><?=$bot_data->levels;?></td>
                <td><?=$bot_data->x_min_lot;?></td>
                <td><?=$bot_data->y_min_lot;?></td>
                <td><?=$bot_data->fee;?></td>
                <td><?=$bot_data->spread;?></td>
                <td><?=$bot_data->coef_lambda;?></td>
                <td><?=$bot_data->coef_RR;?></td>
                <td><?=$p1[0];?></td>
                <td><?=$p1[1];?></td>
                <td><?=$p1[2];?></td>
            <?php }else{ ?>
                <td><?=$bot_data->investBase;?></td>
                <td><?=$bot_data->investQuote;?></td>
                <td><?=$bot_data->add_invest_base;?></td>
                <td><?=$bot_data->add_invest_quote;?></td>
                <td><?=$bot_data->type;?></td>
                <td><?=$bot_data->maxRange;?></td>
                <td><?=$bot_data->minRange;?></td>
                <td><?=$bot_data->takerFee;?></td>
                <td><?=$bot_data->makerFee;?></td>
                <td><?=$bot_data->BaseMinLot;?></td>
                <td><?=$bot_data->QuoteMinLot;?></td>
                <td><?=$bot_data->minLot;?></td>
                <td><?=$bot_data->lambda;?></td>
                <td><?=$bot_data->spread;?></td>
                <td><?=$bot_data->minRRTrailingStop;?></td>
                <td><?=$bot_data->distanceTrailingStop;?></td>
                <td><?=$bot_data->forceMajor;?></td>
                <td><?=$bot_data->minRRAlgorithm;?></td>
                <td><?=$bot_data->distanceAlgorithm;?></td>
            <?php } ?>

                <td><?=$p;?></td>
                <td><?=$p;?></td>
                <td><?=$p;?></td>
                <td><?=$p;?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <?php

}
