<?php
include '../header.php';
require "../../pdo/config.php"; ?>
<div class="container">
    <div class="row">
        <?php if(isset($_GET['bot_id'])) {
            try {
                $connection = new PDO($dsn, $username, $password, $options);
                $id = $_GET['bot_id'];

                $sql = "SELECT * FROM edit WHERE bot_id = :id";
                $statement = $connection->prepare($sql);
                $statement->bindValue(':id', $id);
                $statement->execute();

                $edit = $statement->fetchAll();
        ?>
                <table id="bots" style="text-align: left">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Bot_id</th>
<!--                        <th>Bot_data</th>-->
                        <th>Account</th>
                        <th>Pair</th>
                        <th>Earn</th>
                        <?php
                if(isset($_GET['type']) && $_GET['type'] == 'Old') {

                    ?>
                            <th>Investment</th>
                            <th>Add_asset</th>
                            <th>upper</th>
                            <th>lower</th>
                            <th>levels</th>
                            <th>fee</th>
                            <th>spread</th>
                            <th>coef_lambda</th>
                            <th>coef_RR</th>
                <?php    }else { ?>
                            <th>Invest_BASE</th>
                            <th>Invest_QUOTE</th>
                            <th>Type</th>
                            <th>max_RANGE</th>
                            <th>min_RANGE</th>
                            <th>min_RR_TS</th>
                            <th>Distance_TS</th>
                            <th>min_RR_ALGO</th>
                            <th>Distance_Algo</th>
                <?php    }  ?>
                        <th>bot_info</th>
                        <th>date</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($edit as $row) : ?>

                        <?php

                        if($row["bot_id"] == $_GET['bot_id']) {

                            $bot_data = json_decode($row['bot_data']);
//var_dump($bot_data);
                            $timestamp = strtotime($row['date']);
                            $timestamp += 4*3600;
                            $row['date'] = date('Y-m-d H:i:s', $timestamp);
                            ?>
                            <tr>
                                <td><?= $row["id"]; ?></td>
                                <td><?= $row["bot_id"]; ?></td>
                                <td><?= $bot_data->account ?></td>
                                <td><?= $bot_data->pair ?></td>
                                <td><?= $bot_data->earn ?></td>
                            <?php
                            if(isset($_GET['type']) && $_GET['type'] == 'Old') {

                                ?>
                                <td><?= $bot_data->investment ?></td>
                                <td><?= $bot_data->add_asset ?></td>
                                <td><?= $bot_data->upper_threshold ?></td>
                                <td><?= $bot_data->lower_threshold ?></td>
                                <td><?= $bot_data->levels ?></td>
                                <td><?= $bot_data->fee ?></td>
                                <td><?= $bot_data->spread ?></td>
                                <td><?= $bot_data->coef_lambda ?></td>
                                <td><?= $bot_data->coef_RR ?></td>
                            <?php    } else { ?>
                                <td><?= $bot_data->investBase ?></td>
                                <td><?= $bot_data->investQuote ?></td>
                                <td><?= $bot_data->type ?></td>
                                <td><?= $bot_data->maxRange ?></td>
                                <td><?= $bot_data->minRange ?></td>
                                <td><?= $bot_data->minRRTrailingStop ?></td>
                                <td><?= $bot_data->distanceTrailingStop ?></td>
                                <td><?= $bot_data->minRRAlgorithm ?></td>
                                <td><?= $bot_data->distanceAlgorithm ?></td>
                            <?php    }  ?>

                                <td onclick="set.actionOpenInfo('<?= $row["bot_id"] ?>','<?=$_GET["type"]?>')"><a href="javascript: void(0)">bot_info</a></td>
                                <td><?= $row["date"]; ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    <?php endforeach; ?>
                    </tbody>
                </table>

        <?php
            } catch(PDOException $error) {
                echo '<div class="alert alert-danger alert-dismissible fade in col-sm-6 col-sm-offset-3" style="text-align: center">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Error!</strong> ' . $error->getMessage() . '
                </div>';
            }
?>
            <button type="button" class="hidden" id="button_open" data-toggle="modal" data-target="#myModalOpen"></button>

            <div class="modal fade" id="myModalOpen" role="dialog" style="text-align: left;">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <!-- OPEN MODAL-->
                    <div class="modal-content" style="display: table-row-group">
                        <div class="modal-header" style="border: none">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="open_modal_header"></h4>
                            <div id="ticker_result"></div>
                            <div id="balance_result"></div>

                        </div>

                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#menu0">All Information</a></li>
<!--                            <li><a data-toggle="tab" href="#menu1">userSchedule</a></li>-->
<!--                            <li><a data-toggle="tab" href="#menu2">orderHistory</a></li>-->
<!--                            <li><a data-toggle="tab" href="#menu4">Balance</a></li>-->
                        </ul>

                        <div class="tab-content">

                            <div id="menu0" class="tab-pane fade in active"  style="max-width: 110em; overflow-x: auto;">
                                <div class="modal-body" style="padding-left: 30px;">
                                    <table class="bots" id="all">
                                        <div id="open_tbody_checkbox">
                                        </div>
                                        <tbody id="open_tbody_all">
                                        </tbody>
                                    </table>
                                    <input value="Export as CSV All" style="margin-top: 15px;" type="button" class="btn btn-info" onclick="$('#all').table2CSV({header:['']}, 'all')">
                                </div>
                            </div>

<!--                            <div id="menu1" class="tab-pane fade">-->
<!--                                <div class="modal-body" style="padding-left: 30px;">-->
<!--                                    <table class="bots" id="schedule">-->
<!--                                        <div id="open_tbody_checkbox">-->
<!--                                        </div>-->
<!--                                        <tr>-->
<!--                                            <th>Level</th>-->
<!--                                            <th>Amount</th>-->
<!--                                            <th>Price</th>-->
<!--                                            <th>Total_Value</th>-->
<!--                                            <th>Distribution</th>-->
<!--                                            <th>Cumulative_Distribution</th>-->
<!--                                            <th>Profit</th>-->
<!--                                            <th>Effective_Price</th>-->
<!--                                            <th>RL</th>-->
<!--                                        </tr>-->
<!--                                        <tbody id="open_tbody">-->
<!--                                        </tbody>-->
<!--                                    </table>-->
<!--                                    <input value="Export as CSV Schedule" style="margin-top: 15px;" type="button" class="btn btn-info" onclick="$('#schedule').table2CSV({header:['Level','Amount','Price','Total_Value','Profit','Effective_Price','RL']}, 'schedule')">-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div id="menu2" class="tab-pane fade">-->
<!--                                <div class="modal-body" style="padding-left: 30px;">-->
<!--                                    <table class="bots">-->
<!--                                        <tbody id="open_tbody_history">-->
<!--                                        </tbody>-->
<!--                                    </table>-->
<!--                                    <a type="submit" class="btn btn-info" style="margin-top: 15px;"  onclick="set.downloadButton('orderHistory')">Export as CSV History</a>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div id="menu4" class="tab-pane fade">-->
<!--                                <div class="modal-body" style="padding-left: 30px;">-->
<!--                                    <table class="bots" id="state_summary">-->
<!--                                        <tr>-->
<!--                                            <th>Y</th>-->
<!--                                            <th>X</th>-->
<!--                                            <th>Left Investment</th>-->
<!--                                            <th>Initial Investment</th>-->
<!--                                            <th>Total Investment in Current Price</th>-->
<!--                                            <th>Profit in Current Price</th>-->
<!--                                            <th>Efficiency %</th>-->
<!--                                        </tr>-->
<!--                                        <tbody id="open_tbody_stateSummary">-->
<!--                                        </tbody>-->
<!--                                    </table>-->
<!--                                    <input value="Export as CSV Balance" style="margin-top: 15px;" type="button" class="btn btn-info" onclick="$('#state_summary').table2CSV({header:['Y', 'X', 'Left Investment', 'Initial Investment', 'Total Investment in Current Price', 'Profit in Current Price', 'Efficiency %']}, 'Balance')">-->
<!--                                </div>-->
<!--                            </div>-->


                        </div>

                        <input type="hidden" id="open_bot">
                        <div class="modal-footer">
                            <!--                    <a type="submit" class="btn btn-info" style="float:left;"  onclick="set.downloadButton('userSchedule')">Download Schedule</a>-->
<!--                            <button class="btn btn-danger" id="close_position" onclick="set.lineClose(this)">close positions</button>-->
                            <button type="button" id="close_modal" class="btn btn-secondary" data-dismiss="modal">Cancel
                            </button>

                        </div>
                    </div>
                    <!--END OPEN MODAL-->

                </div>
            </div>

            <?php
include '../../footer.php';

        exit; } elseif($_POST['id']) {
            try {
                $connection = new PDO($dsn, $username, $password, $options);
                $id = $_POST['id'];

                $sql = "SELECT * FROM edit WHERE user_id = :id";
                $statement = $connection->prepare($sql);
                $statement->bindValue(':id', $id);
                $statement->execute();

                $edit = $statement->fetchAll();

                        ?>

                        <table id="bots" style="text-align: left">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Bot_id</th>
                                <th>Account</th>
                                <th>Type</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($edit as $row) : ?>

                                <?php
                                $type = 'Old';

                                $bot_data = json_decode($row['bot_data']);
//                                echo "<pre>";
//                                print_r($bot_data->distanceAlgorithm);
//                                echo "</pre>";

                            if($bot_data->distanceAlgorithm != null){
                                $type = 'New';
                            }

                                ?>
                                <tr>
                                    <td><?= $row["id"]; ?></td>
                                    <td><a href="?bot_id=<?= $row["bot_id"]; ?>&type=<?=$type?>"><?= $row["bot_id"]; ?></a></td>
                                    <td><?= $bot_data->exchange; ?></td>
                                    <td><?=$type?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>

                <?php
            } catch(PDOException $error) {
                echo '<div class="alert alert-danger alert-dismissible fade in col-sm-6 col-sm-offset-3" style="text-align: center">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Error!</strong> ' . $error->getMessage() . '
                </div>';
            }
exit; } ?>

        </div>
    </div>

