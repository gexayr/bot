<?php include '../header.php';
echo '<div><h2 style="text-align: center">Users</h2></div>
<div class="container">

    <div class="row">
    
';

require "../../pdo/config.php";
require "../../pdo/common.php";


if (isset($_POST['submit'])) {
    if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();
    $_POST['account'] = json_encode($_POST['account']);
//

    try {
        $connection = new PDO($dsn, $username, $password, $options);

        $user =[
            "id"        => $_POST['id'],
            "login" => $_POST['login'],
            "role"  => $_POST['role'],
            "account"       => $_POST['account'],
            "date"      => $_POST['date']
        ];

        $sql = "UPDATE users 
            SET login = :login, 
              role = :role, 
              account = :account, 
              date = :date 
            WHERE id = :id";

        $statement = $connection->prepare($sql);
        $statement->execute($user);
        header('Location: /admin/accounts/');
    } catch(PDOException $error) {
        echo ' <div class="alert alert-danger alert-dismissible fade in col-sm-6 col-sm-offset-3" style="text-align: center">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong> ' . $error->getMessage() . '
        </div>';
    }
    die;
}

if (isset($_GET['id'])) {
    try {
        $connection = new PDO($dsn, $username, $password, $options);
        $id = $_GET['id'];

        $sql = "SELECT * FROM users WHERE id = :id";
        $statement = $connection->prepare($sql);
        $statement->bindValue(':id', $id);
        $statement->execute();

        $user = $statement->fetch(PDO::FETCH_ASSOC);


        $allow_accounts = json_decode($user['account']);
        foreach ($allow_accounts as $key=>$value) {
            $allow[] = $key;
        }

        $dir_exchange = $_SERVER['DOCUMENT_ROOT'] . '/base/';
        $exchange_folders = scandir($dir_exchange);

        array_shift($exchange_folders);
        array_shift($exchange_folders);

        $all_accounts = '';
        foreach ($exchange_folders as $exchange) {

            $dir_key = $_SERVER['DOCUMENT_ROOT'] . '/base/' . $exchange . '/';
            $key_folders = scandir($dir_key);

            array_shift($key_folders);
            array_shift($key_folders);

            foreach ($key_folders as $account) {
                $checked = '';
                if (in_array($account, $allow)) {
                    $checked = 'checked';
                }
                $all_accounts .= "<label><input type='checkbox' class='form-check-input' $checked name='account[$account]'> $account </label><br>";
            }
        }


    } catch(PDOException $error) {
        echo ' <div class="alert alert-danger alert-dismissible fade in col-sm-6 col-sm-offset-3" style="text-align: center">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong> ' . $error->getMessage() . '
        </div>';
        exit;
    }

    ?>


    <h2>Edit a user</h2>

    <form method="post" class="form-group col-sm-4 col-sm-offset-4">
        <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">
        <br>

        <label style="float: left">ID</label>
        <input name="id" type="text" value="<?=$user["id"]?>" readonly class="form-control">
        <br>

        <label style="float: left">Login</label>
        <input name="login" type="text" value="<?=$user["login"]?>" class="form-control">
        <br>

        <label style="float: left">Role</label>
        <select name="role" id="" required class="form-control">
            <option value="">select...</option>
            <option value="admin" <?php if($user["role"] == 'admin') echo 'selected';?>>Admin</option>
            <option value="user" <?php if($user["role"] == 'user') echo 'selected';?>>User</option>
        </select>
        <br>
        <div class="" style="text-align: left; margin: 20px auto"> <!-- State Button -->
            <strong>Accounts</strong>
            <div id="account">
                <?=$all_accounts;?>
            </div>
        </div>

        <label style="float: left">Registaration Date</label>
        <input name="date" type="text" value="<?=$user["date"]?>" readonly class="form-control">
        <br>
        <input type="submit" name="submit" value="Submit" class="btn btn-info">
    </form>
    </form>
    <script src="../../script.js"></script>

    <?php
    exit;
}

try {
    $connection = new PDO($dsn, $username, $password, $options);
    $sql = "SELECT * FROM users";

    $statement = $connection->prepare($sql);
    $statement->execute();

    $result = $statement->fetchAll();
} catch (PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
}
?>

<?php
if ($result && $statement->rowCount() > 0) { ?>

    <table  id="bots" style="text-align: left">
        <thead>
        <tr>
            <th>#</th>
            <th>Login</th>
            <th>Role</th>
            <th>Date</th>
            <th>Account</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($result as $row) : ?>
            <?php

            $allow_accounts = json_decode($row['account']);

            ?>
            <tr>
                <td><?php echo escape($row["id"]); ?></td>
                <td><?php echo escape($row["login"]); ?></td>
                <td><?php echo escape($row["role"]); ?></td>
                <td><?php echo escape($row["date"]); ?></td>
                <td>
                    <?php
                    foreach ($allow_accounts as $key=>$value) {
                        echo $key . ' | ';
                    }
                    ?>
                </td>
                <td>

                    <span style="display: inline-flex">
                            <form method="post" style="text-align: center" action="/admin/accounts/info.php">
                                <input type="hidden" name="id" value="<?=$row['id'];?>">
                                <input type="submit" value="Info" class="btn btn-info">
                            </form>
                        &nbsp
                        <a href="?id=<?php echo escape($row["id"]); ?>" class="btn btn-info">Edit</a>&nbsp;
                        <a href="javascript: void(0)" class="btn btn-danger"  onclick="set.remUser(<?=$row['id']?>, this)">Remove</a>
                    </span>

                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php } else { ?>
    <div class="alert alert-danger alert-dismissible fade in col-sm-6 col-sm-offset-3" style="text-align: center">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!</strong> <?=$error->getMessage();?>
    </div>
<?php }
?>
</div>
</div>

<?php include '../../footer.php'; ?>
