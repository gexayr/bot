var set = {
    ajaxMethod: 'POST',


    ChangePair: function() {
        var pair = ($('#pair').val());

        var part_1 = pair.substring(0, 3);
        var part_2 = pair.substring(3, 6);
        if(pair == 'DASHUSD' || pair == 'DASHEUR'){
            var part_1 = pair.substring(0, 4);
            var part_2 = pair.substring(4, 7);
        }else if(pair == 'BCHABCUSD' || pair == 'BCHABCBTC' || pair == 'BCHABCUSDT'){
            var part_1 = pair.substring(0, 6);
            var part_2 = pair.substring(6, 9);
        }else if(pair == 'BCHSVUSD' || pair == 'BCHSVBTC' || pair == 'BCHSVUSDT'){
            var part_1 = pair.substring(0, 5);
            var part_2 = pair.substring(5, 9);
        }else if(pair == 'BCHABC-USDT'){
            var part_1 = pair.substring(0, 5);
            var part_2 = pair.substring(7, 9);
        }else if(pair == 'BCHSV-USDT'){
            var part_1 = pair.substring(0, 5);
            var part_2 = pair.substring(7, 15);
        }

        $('#Y').text(part_1);
        $('#X').text(part_2);
    },

    getTime: function() {

        $.ajax({
            url: '/time.php',
            type: this.ajaxMethod,
            data: '',
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
            },
            success: function(result){
                if ( $('#step_1').attr('style') == 'display: none;' ) {
                } else {
                    $('#name_id').val(result);
                }

            }
        });
    },

    ChangeAccOption: function() {

        var option = ($('#acc_option').val());

        console.log(option);
        window.location.href = '/?filter=' + option;

    },

    ChangeExchange: function() {

        var exchange = ($('#exchange_key').val());

        $('#name_key').val(exchange + '_');
        if(exchange == 'df'){
            $('#secret').prop('type', 'password');
            $('#key').prop('type', 'email');
        }else{
            $('#secret').prop('type', 'text');
            $('#key').prop('type', 'text');
        }
    },
        GetExchange: function() {

            var formData = new FormData();
            var exchange = ($('#exchange').val());
            formData.append('exchange', exchange);
            $.ajax({
                url: '/account.php',
                type: this.ajaxMethod,
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function(){
                },
                success: function(result){
                    $('#account').html(result);
                }
            });
            $.ajax({
                url: '/tickers.php',
                type: this.ajaxMethod,
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function(){
                },
                success: function(result){
                    $('#pair').html(result);
                }
            });
        },

    GetExchangeAdmin: function() {

        var formData = new FormData();
        var exchange = ($('#exchange').val());
        formData.append('exchange', exchange);
        formData.append('for_admin', true);
        $.ajax({
            url: '/account.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
            },
            success: function(result){

                $('#account').append(result);
            }
        });
    },

    actionEdit: function (id) {
        $('#open_bot').val(id);

        $('#name_id_edit').val('');
        $('#exchange_edit').val('');
        $('#account_edit').val('');
        $('#account_edit').val('');
        $('#pair_edit').val('');
        $('#earn_edit').val('');
        $('#investment_edit').val('');
        $('#add_asset_edit').val('');
        $('#upper_threshold_edit').val('');
        $('#lower_threshold_edit').val('');
        $('#levels_edit').val('');
        $('#x_min_lot_edit').val('');
        $('#y_min_lot_edit').val('');
        $('#fee_edit').val('');
        $('#spread_edit').val('');
        $('#coef_lambda_edit').val('');
        $('#coef_RR_edit').val('');
        $.ajax({
            cache: false,
            url: '/bot/'+ id + '/' + id + '.json',
            data: '',
            success: function( result ) {
                // console.log(result);
                $('#name_id_edit').val(result.id);
                $('#exchange_edit').val(result.exchange);
                $('#account_edit').val(result.account);
                $('#pair_edit').val(result.pair);
                $('#earn_edit').val(result.earn);
                $('#investment_edit').val(result.investment);
                $('#upper_threshold_edit').val(result.upper_threshold);
                $('#lower_threshold_edit').val(result.lower_threshold);
                $('#levels_edit').val(result.levels);
                $('#x_min_lot_edit').val(result.x_min_lot);
                $('#y_min_lot_edit').val(result.y_min_lot);
                $('#fee_edit').val(result.fee);
                $('#spread_edit').val(result.spread);
                $('#coef_lambda_edit').val(result.coef_lambda);
                $('#coef_RR_edit').val(result.coef_RR);
            }
        });
    },


    // userSchedule modal

    actionOpen: function (id) {
        $('#button_open').click();
        $('#open_modal_header').text(id);
        $('#edit_stop_button').text(id);
        $('#open_bot').val(id);
        var count = 1;
        var html ='';
        var loader = "<div class='loader_block'><i class='fas fa-sync fa-spin'></i></div>";
        $('#open_tbody_history').html(loader);
        $('#open_tbody_stateSummary').html(loader);
        $('#open_tbody').html(loader);
        // $('#open_tbody_checkbox').html(loader);
        $('#newOrderInfo_result').html(loader);

        var formData = new FormData();
        formData.append('id', id);

/*LOAD ORDER HISTORY*/
        // // if(UrlExists('bot/'+id+'/orderHistory.csv')){
        //     show_(id,count,'orderHistory', '#open_tbody_history');
        // // };


        show_orderHistoryTab(1);
        function show_orderHistoryTab(count) {
            formData.append('id', id);
            formData.append('file', 'orderHistory');
            $.ajax({
                url: 'csv_read.php',
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {

                },
                success: function (result) {
                    // console.log(result);
                    // html = result;
                    if (result == '') {
                        console.log('orderHistory not load '+ count);
                        if(count < 30){
                            count++;
                            show_orderHistoryTab(count)
                        }else{
                            $('#open_tbody_history').html(result);
                        }
                    }else{
                        $('#open_tbody_history').html(result);
                    }
                }
            });

        }
/* END LOAD ORDER HISTORY*/

/*LOAD stateSummary*/
        // // if(UrlExists('bot/'+id+'/stateSummary.csv')){
        //     show_(id,count,'stateSummary', '#open_tbody_stateSummary');
        // // };


        show_stateSummaryTab(1);
        function show_stateSummaryTab(count) {
            formData.append('id', id);
            formData.append('file', 'stateSummary');
            $.ajax({
                url: 'csv_read.php',
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {

                },
                success: function (result) {
                    // console.log(result);
                    // html = result;
                    if (result == '') {
                        console.log('stateSummary not load '+ count);
                        if(count < 30){
                            count++;
                            show_stateSummaryTab(count)
                        }else{
                            $('#open_tbody_stateSummary').html(result);
                        }
                    }else{
                        $('#open_tbody_stateSummary').html(result);
                    }
                }
            });
        }

/*END LOAD stateSummary*/

/*LOAD schedule AND checkboxes*/
        // // if(UrlExists('bot/'+id+'/userSchedule.csv')){
        // //     show_(id,count,'userSchedule', '#open_tbody');
        //     show_(id,count,'userSchedule', '#open_tbody',show_checkbox(id,count));
        // // };


        show_userScheduleTab(1);
        function show_userScheduleTab(count) {
            formData.append('id', id);
            formData.append('file', 'userSchedule');
            $.ajax({
                url: 'csv_read.php',
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {

                },
                success: function (result) {
                    // console.log(result);
                    // html = result;
                    if (result == '') {
                        console.log('userSchedule not load '+ count);
                        if(count < 30){
                            count++;
                            show_userScheduleTab(count)
                        }else{
                            $('#open_tbody').html(result);
                        }
                    }else{
                        $('#open_tbody').html(result);
                    }
                }
            });

        }

        // show_checkbox(id, count)
        function show_checkbox(id, count) {
            var html_ = '';

                $.ajax({
                    cache: false,
                    url: '/bot/' + id + '/userSchedule.csv',
                    data: '',
                    success: function (result) {
                        if (result == '') {
                            console.log('checkbox not load '+ count);
                            count++
                            if(count < 50){
                                show_checkbox(id, count)
                            }
                        }
                        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                        var levels_arr = (csv2array(result));
                        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                        var levels = levels_arr.length;

                        for (var i = levels; i > 0; i--) {
                            var element = (levels_arr[i]);
                            var new_data_check = '<div class="checkbox_blocks">\n';
                            new_data_check += '' +
                                ' ' +
                                '     <input type="checkbox" class="form-check-input" data-id="' + (i-1) + '"  name="line[' + (i-1) + ']">' +
                                ' ' +
                                '</div>';

                            html_ = html_ + new_data_check;
                            $('#open_tbody_checkbox').html(html_);
                        }
                        // console.log(html_);

                    }
                });
            $('#open_tbody_checkbox').html(html_);

        }
/*END LOAD schedule AND checkboxes*/


/*GET TICKERS*/

        var exchange = '';
        var pair = '';


        $.ajax({
            url: 'bot/'+ id + '/' + id + '.json',
            data: '',
            cache: false,
            processData: false,
            contentType: false,
            success: function( result ) {
                // console.log(result);
                exchange = result.exchange;
                pair = result.pair;


                formData.append('exchange_', exchange);
                formData.append('pair', pair);
                getTickers(exchange,pair,count);

            }
        });

        function getTickers(exchange, pair, count) {
            $.ajax({
                url: '/tickers.php',
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {
                },
                success: function (result) {
                    // console.log(result);
                    if (result == "<div class='tickers'><span>ask:</span> <span> bid:</span>  <span>Date:</span> 01-01 04:00:00</div>") {
                        if (count < 40) {
                            console.log('getTick ' + count);
                            count++;
                            getTickers(exchange, pair, count);
                        }
                    }
                    html = result;
                    $('#ticker_result').html(html);
                }
            });
        }
/*END GET TICKERS*/

        formData.append('file', 'newOrderInfo');

        $.ajax({
            url: '/csv_read.php',
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function( result ) {
                // console.log(result);
                $('#newOrderInfo_result').html(result);

            }
        });

    },


    lineClose: function () {

        if (!confirm("Are you sure ?")) {
            return false
        }


        var bot = $('#open_bot').val();
        var element = $('.form-check-input');

        var formData = new FormData();
        formData.append('id', bot);
        for (var i=0;i<element.length;i++){
            var line = element[i];
            if ($(element[i]).is(':checked')) {
                formData.append($(line).data("id"), 1);
            } else {
                formData.append($(line).data("id"), 0);
            }
        }
        $.ajax({
            url: '/closeList.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){

            },
            success: function(result){
                if(result == 'success'){
                    location.reload();
                }
            }
        });

    },


    actionPause: function (id, element) {


        var formData = new FormData();
        formData.append('id', id);
        var text = $(element).text();
        var text = $.trim(text);
        if(text == "Pause"){
            formData.append('action', 'pause');
            $(element).text( text == "Activate" ? "Pause" : "Activate");
        }else{
            formData.append('action', 'activate');
            $(element).text( text == "Pause" ? "Activate" : "Pause");
        }

        $.ajax({
            url: '/pause.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){

            },
            success: function(result){
                if(result == 'success'){
                    $(element).toggleClass('btn-info btn-success');
                }
            }
        });
    },


    actionOrder: function (id, element) {

        var formData = new FormData();
        formData.append('id_order', id);

        $.ajax({
            url: '/pause.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){

            },
            success: function(result){
                if(result == 'success'){
                    $(element).toggleClass('btn-info btn-success');
                }
            }
        });
    },


    actionRemove: function (id, element) {

        if (!confirm("Are you sure ?")) {
            return false
        }

        var rem_element = $(element).parent().parent();
        rem_element.fadeOut();
        $('#myModalOpen').hide();
        var formData = new FormData();
        formData.append('id', id);
        $.ajax({
            url: '/remove.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function(result){
            }
        });
    },

    stopButton: function (element) {

        if (!confirm("Are you sure ?")) {
            return false
        }
        var id = $('#open_bot').val();
        var formData = new FormData();
        formData.append('id', id);
        $.ajax({
            url: '/stop.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('#waiting').css('display', 'block');
            },
            success: function(result){
                $('#waiting').css('display', 'none');
            }
        });
    },

    stopButton_: function (id, element) {

        if (!confirm("Are you sure ?")) {
            return false
        }
        console.log(element);
        $(element).fadeOut();

        var formData = new FormData();
        formData.append('id', id);
        $.ajax({
            url: 'stop.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('#waiting').css('display', 'block');
            },
            success: function(result){
                if(result == 'success'){
                    location.reload();
                }
            }
        });
    },


    copyButton: function (user_id) {

        if (!confirm("Are you sure ?")) {
            return false
        }
        var id = $('#open_bot').val();

        console.log(id);
        $.ajax({
            cache: false,
            url: '/bot/'+ id + '/' + id + '.json',
            data: '',
            success: function( jsondata ) {
                console.log(jsondata);

                $.ajax({
                    url: '/time.php',
                    type: this.ajaxMethod,
                    data: '',
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function(new_id){
                        $('#name_id_edit').val(new_id);
                        $('#exchange_edit').val(jsondata.exchange);
                        $('#account_edit').val(jsondata.account);
                        $('#pair_edit').val(jsondata.pair);
                        $('#earn_edit').val(jsondata.earn);

                        $('#add_asset_for').hide();
                        $('#investment_edit').removeAttr('readonly');
                        $('#hide_for_copy').html('<button type="submit" class="btn btn-info" onclick="set.createCopyButton(' + user_id + ')">Send</button>');

                        var formData = new FormData();

                        formData.append('id', new_id);
                        formData.append('exchange', jsondata.exchange);
                        formData.append('account', jsondata.account);
                        formData.append('pair', jsondata.pair);
                        formData.append('earn', jsondata.earn);

                        $.ajax({
                            url: '/next.php',
                            type: 'POST',
                            data: formData,
                            cache: false,
                            processData: false,
                            contentType: false,
                            success: function(result){
                                // console.log(result);
                            }
                        });

                    }
                });
            }
        });
    },

    createCopyButton: function (user) {

        var investment = $('#investment_edit').val();
        var upper_threshold = $('#upper_threshold_edit').val();
        var lower_threshold = $('#lower_threshold_edit').val();
        var levels = $('#levels_edit').val();
        var x_min_lot = $('#x_min_lot_edit').val();
        var y_min_lot = $('#y_min_lot_edit').val();
        var fee = $('#fee_edit').val();
        var spread = $('#spread_edit').val();
        var coef_lambda = $('#coef_lambda_edit').val();
        var coef_RR = $('#coef_RR_edit').val();


        if(
            upper_threshold<lower_threshold
        ){
            alert("upper threshold can't be small than lower threshold");
            return false
        }



        if(
            y_min_lot<0.003
        ){
            alert("Y min lot can't be small than 0.003");
            return false
        }


        if(
            name_id.length<=0 ||
            account.length<=0 ||
            pair.length<=0 ||
            earn.length<=0 ||
            investment.length<=0 ||
            upper_threshold.length<=0 ||
            lower_threshold.length<=0 ||
            levels.length<=0 ||
            x_min_lot.length<=0 ||
            y_min_lot.length<=0 ||
            fee.length<=0 ||
            spread.length<=0 ||
            coef_lambda.length<=0 ||
            coef_RR.length<=0
        ){
            alert('Missing data');
            return false
        }


        var formData = new FormData();

        formData.append('id', $('#name_id_edit').val());
        formData.append('exchange', $('#exchange_edit').val());
        formData.append('account', $('#account_edit').val());
        formData.append('pair', $('#pair_edit').val());
        formData.append('earn', $('#earn_edit').val());
        formData.append('investment', $('#investment_edit').val());
        formData.append('upper_threshold', $('#upper_threshold_edit').val());
        formData.append('lower_threshold', $('#lower_threshold_edit').val());
        formData.append('levels', $('#levels_edit').val());
        formData.append('x_min_lot', $('#x_min_lot_edit').val());
        formData.append('y_min_lot', $('#y_min_lot_edit').val());
        formData.append('fee', $('#fee_edit').val());
        formData.append('spread', $('#spread_edit').val());
        formData.append('coef_lambda', $('#coef_lambda_edit').val());
        formData.append('coef_RR', $('#coef_RR_edit').val());

        $.ajax({
            url: '/result.php',
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $('#waiting').css('display', 'block');
            },
            success: function(result){
                // console.log(result);
            }
        });

        formData.append('user', user);
        formData.append('bot_type', 'classic');

        $.ajax({
            url: '/data/entry.php',
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function(result){
                console.log(result);
                location.reload();

            }
        });

        setTimeout(function(){
            // window.location.href = '/'
            location.reload();
        }, 3000);



    },

    saveButton: function () {
        var formData = new FormData();

        var name_id = $('#name_id_edit').val();
        var exchange = $('#exchange_edit').val();
        var account = $('#account_edit').val();
        var pair = $('#pair_edit').val();
        var earn = $('#earn_edit').val();
        var investment = $('#investment_edit').val();
        var add_asset = $('#add_asset_edit').val();
        var upper_threshold = $('#upper_threshold_edit').val();
        var lower_threshold = $('#lower_threshold_edit').val();
        var levels = $('#levels_edit').val();
        var x_min_lot = $('#x_min_lot_edit').val();
        var y_min_lot = $('#y_min_lot_edit').val();
        var fee = $('#fee_edit').val();
        var spread = $('#spread_edit').val();
        var coef_lambda = $('#coef_lambda_edit').val();
        var coef_RR = $('#coef_RR_edit').val();


        if(
            upper_threshold<lower_threshold
        ){
            alert("upper threshold can't be small than lower threshold");
            return false
        }

        if(
            exchange.length<=0 ||
            upper_threshold.length<=0 ||
            lower_threshold.length<=0 ||
            levels.length<=0 ||
            x_min_lot.length<=0 ||
            y_min_lot.length<=0 ||
            fee.length<=0 ||
            spread.length<=0 ||
            coef_lambda.length<=0 ||
            coef_RR.length<=0
        ){
            alert('Missing data');
            return false
        }


        if(
            y_min_lot<0.003
        ){
            alert("Y min lot can't be small than 0.003");
            return false
        }

        formData.append('id', name_id);
        formData.append('exchange', exchange);
        formData.append('account', account);
        formData.append('pair', pair);
        formData.append('earn', earn);
        formData.append('investment', investment);
        formData.append('add_asset', add_asset);
        formData.append('upper_threshold', upper_threshold);
        formData.append('lower_threshold', lower_threshold);
        formData.append('levels', levels);
        formData.append('x_min_lot', x_min_lot);
        formData.append('y_min_lot', y_min_lot);
        formData.append('fee', fee);
        formData.append('spread', spread);
        formData.append('coef_lambda', coef_lambda);
        formData.append('coef_RR', coef_RR);

        $.ajax({
            url: '/edit.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $('#waiting').css('display', 'block');
            },
            success: function(result){
                if(result == 'success'){
                    // window.location.href = '/'
                    location.reload();
                }
            }
        });

        var user = $('#user_id').val();
        formData.append('user', user);
        formData.append('bot_type', 'classic');

        $.ajax({
            url: '/data/update.php',
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function(result){
                if(result == 'success'){
                    location.reload();
                }
            }
        });
    },

    addKeyButton: function () {

        var name_key = $('#name_key').val();
        var key = $('#key').val();
        var secret =  $('#secret').val();
        var exchange =  $('#exchange_key').val();



        if(name_key.length <3 ){
            alert('name is too short')
            return false
        }else if(key.length <4){
            alert('key is too short')
            return false
        }else if(secret.length <4 && exchange != 'df'){
            alert('secret is too short')
            return false
        }


        var formData = new FormData();
        formData.append('exchange', exchange);
        formData.append('name', name_key);
        formData.append('key', key);
        formData.append('secret',secret);
        $.ajax({
            url: '/key.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function(result){
// console.log(result);
                if(result == 'success'){
                    alert('account successfully created')
                    window.location.href = '/'
                }else if(result == 'isset'){
                    alert('account with this name already exists')
                }

            }
        });
    },


    downloadButton: function (file) {
        var id = $('#open_bot').val();

        window.location.href = '/download.php?id='+id+'&file='+file;
    },


    actionRemoveKey: function (exchange, name, element) {
        if (!confirm("Are you sure ?")) {
            return false
        }

        var rem_element = $(element).parent().parent();
        rem_element.fadeOut();

        var formData = new FormData();
        formData.append('name', name);
        formData.append('exchange', exchange);
        $.ajax({
            url: '/rem_key.php',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function(result){
                if(result == 'success'){
                    alert('key deleted successfully');
                }
            }
        });
    },


};

$("#next").click(function(){

    var name_id = $('#name_id').val();
    var exchange = $('#exchange').val();
    var account = $('#account').val();
    var pair = $('#pair').val();
    var earn = $('#earn').val();

    if(account.length <2 ){
        alert('Account is empty');
        return false
    }else if(pair.length <5){
        alert('Pair is empty');
        return false
    }

    if(exchange.length<2){
        alert('Exchange is empty');
        return false
    }

    $('#step_1').hide();
    $('#step_2').show();
    $('#modal_footer').show();

    var formData = new FormData();

    formData.append('id', name_id);
    formData.append('exchange', exchange);
    formData.append('account', account);
    formData.append('pair', pair);
    formData.append('earn', earn);

    $.ajax({
        url: '/next.php',
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        success: function(result){
            // console.log(result);
        }
    });
});

$("#send").click(function(){
    var formData = new FormData();

    var name_id = $('#name_id').val();
    var exchange = $('#exchange').val();
    var account = $('#account').val();
    var pair = $('#pair').val();
    var earn = $('#earn').val();
    var investment = $('#investment').val();
    var upper_threshold = $('#upper_threshold').val();
    var lower_threshold = $('#lower_threshold').val();
    var levels = $('#levels').val();
    var x_min_lot = $('#x_min_lot').val();
    var y_min_lot = $('#y_min_lot').val();
    var fee = $('#fee').val();
    var spread = $('#spread').val();
    var coef_lambda = $('#coef_lambda').val();
    var coef_RR = $('#coef_RR').val();


    if(
        upper_threshold<lower_threshold
    ){
        alert("upper threshold can't be small than lower threshold");
        return false
    }
    if(
        y_min_lot<0.003
    ){
        alert("Y min lot can't be small than 0.003");
        return false
    }

    if(
        name_id.length<=0 ||
        account.length<=0 ||
        pair.length<=0 ||
        earn.length<=0 ||
        investment.length<=0 ||
        upper_threshold.length<=0 ||
        lower_threshold.length<=0 ||
        levels.length<=0 ||
        x_min_lot.length<=0 ||
        y_min_lot.length<=0 ||
        fee.length<=0 ||
        spread.length<=0 ||
        coef_lambda.length<=0 ||
        coef_RR.length<=0
    ){
        alert('Missing data');
        return false
    }



    formData.append('id', $('#name_id').val());
    formData.append('exchange', $('#exchange').val());
    formData.append('account', $('#account').val());
    formData.append('pair', $('#pair').val());
    formData.append('earn', $('#earn').val());
    formData.append('investment', $('#investment').val());
    formData.append('upper_threshold', $('#upper_threshold').val());
    formData.append('lower_threshold', $('#lower_threshold').val());
    formData.append('levels', $('#levels').val());
    formData.append('x_min_lot', $('#x_min_lot').val());
    formData.append('y_min_lot', $('#y_min_lot').val());
    formData.append('fee', $('#fee').val());
    formData.append('spread', $('#spread').val());
    formData.append('coef_lambda', $('#coef_lambda').val());
    formData.append('coef_RR', $('#coef_RR').val());

    $.ajax({
        url: '/result.php',
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {
            $('#waiting').css('display', 'block');
        },
        success: function(result){
            console.log(result);
        }
    });
    var user_id = $('#user_id').val();

    formData.append('user', user_id);
    formData.append('bot_type', 'classic');
    $.ajax({
        url: '/data/entry.php',
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        success: function(result){
            console.log(result);
            location.reload();

        }
    });

    setTimeout(function(){
        location.reload();
    }, 3000);

});

// function UrlExists(url)
// {
//     var http = new XMLHttpRequest();
//     http.open('HEAD', url, false);
//     http.send();
//     return http.status!=404;
// }


function show_(id, count, file, element, func=false)
{
    var loader = "<div class='loader_block'><i class='fas fa-sync fa-spin'></i></div>";
    $(element).html(loader)

    var formData = new FormData();

        formData.append('id', id);
        formData.append('file', file);

        $.ajax({
            url: '/csv_read.php',
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {

            },
            success: function (result) {
                if (result == '') {
                    console.log(file + ' not load '+ count);
                    count++;
                    if(count < 10){
                        show_(id, count, file, element)
                    }
                }else{
                    func;
                    $(element).html(result)
                }
            }
        });
}
