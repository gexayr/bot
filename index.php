<?php include './header.php'; ?>
<?php
$login = $_COOKIE['login'];
if($login != null){
    ?>
    <div class="container">
        <div class="row">
            <h1 style="text-align: center"><a href="app.php" style="text-decoration: none">App</a></h1>
        </div>
    </div>
<?php } ?>
<?

$dir    = $_SERVER['DOCUMENT_ROOT'] . '/bot/';
$files = scandir($dir);

array_shift($files);
array_shift($files);


///*GET USER*/

if(isset($_COOKIE['login'])){

    require "./pdo/config.php";

    try  {
        $connection = new PDO($dsn, $username, $password, $options);

        $sql = "SELECT * 
            FROM users
            WHERE login = :login";

        $login = $_COOKIE['login'];
        $password = $_COOKIE['key'];

        if($_COOKIE['key'] != md5(($_COOKIE['login']))) die();
        $statement = $connection->prepare($sql);
        $statement->bindParam(':login', $login, PDO::PARAM_STR);
        $statement->execute();

        $result = $statement->fetchAll();

        $allow_accounts = json_decode($result[0]['account']);
        $user_id = $result[0]['id'];

        $options_allow = '';
        foreach ($allow_accounts as $key=>$value) {
            $selected = '';
            if(isset($_GET['filter']) && $_GET['filter'] == $key){
                $selected = 'selected';
            }
            $allow[] = $key;

            $options_allow .= "<option value='$key' $selected>$key</option>";
        }

    } catch(PDOException $error) {
        echo ' <div class="alert alert-danger alert-dismissible fade in col-sm-6 col-sm-offset-3" style="text-align: center">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong> ' . $error->getMessage() . '
        </div>';
    }
}

///*END GET USER*/

?>


<div class="container">
    <div class="form-group" style="text-align: right">
        <?php
        $login = $_COOKIE['login'];
        if($login != null){
            ?>
            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" onclick="set.getTime()">
<!--       <span  class="snow_img" style="
    height: 75px;
    background-image: url(/img/1112.png);
    width: 152px;
    margin-left: -39px;
    margin-top: -37px;"
       ></span>-->
                Create New</button>
            <input type="hidden" id="user_id" value="<?=$user_id;?>">

        <?php } ?>
    </div>
    <table id="bots">
        <tr>
            <th>Name</th>
            <th style="text-align: right">
                <select name="acc_option" id="acc_option" class="acc_options" onchange="set.ChangeAccOption()">
                    <option value="">Account</option>
                    <?=$options_allow;?>
                </select>
            </th>
            <th style="text-align: right">Pair</th>
            <th style="text-align: right; width: 87px;">Earn</th>
            <th style="text-align: right">Total Investment</th>
            <th style="text-align: right">Range</th>
            <th style="text-align: right">Status</th>
            <th style="text-align: right">Action</th>
        </tr>
        <button type="button" class="hidden" id="button_open" data-toggle="modal" data-target="#myModalOpen"></button>


        <?php
        function getTicker($exchange, $pair, $counter = 1){
            $dir_ticker = '/home/talisant/tickers/' . $exchange . '/';
            $in_ticker_folder = scandir($dir_ticker . $pair);
                $str = file_get_contents($dir_ticker . $pair . '/ticker.json');
                if(($str == '' && $counter < 40)){
                    sleep(1);
                    $counter++;
                    getTicker($exchange, $pair, $counter);
                }
            return $str;
        }

//        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

        $comand_all =  shell_exec("ps -ef | grep talisant/AlgoXY_new" );
        $comand_all_arr = explode("\n", $comand_all);
        $comand_all_tick =  shell_exec("ps -ef | grep talisant/tickers/" );
        $comand_all_arr_tickers = explode("\n", $comand_all_tick);

        foreach($comand_all_arr as $item){

            $pos_edit = strpos($item, 'AlgoXY_new/Edit.py');
            $pos_order = strpos($item, 'order');

            if($pos_edit != false){
                $comand_all_arr_edit[] = $item;
            }
            if($pos_order != false){
                $comand_all_arr_order[] = $item;
            }

        }

//        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

        
        foreach ($files as $folder) {
            $dir    = $_SERVER['DOCUMENT_ROOT'] . "/bot/$folder";
            $files_ = scandir($dir);
            $style = 'btn-primary';
            $pause_class = "btn-info";
            $pause_content = "Pause";
            if (!in_array("remove", $files_)) {

                if (in_array("pause", $files_)) {
                    $pause_class = "btn-success";
                    $pause_content = "Activate";
                }
                $json = file_get_contents("bot/$folder/$folder.json");
                $data = (json_decode($json));
                $account = $data->account;


                if (in_array($account, $allow)) {

                    if(isset($_GET['filter']) && $_GET['filter'] != '' && $_GET['filter'] != $account){
                        continue;
                    } else {

                        $pair = $data->pair;
                        $exchange = $data->exchange;
                        $total_investment = $data->investment;

//      ================================================================================
//                        OUT OF RANGE
//      ================================================================================
                        $out_of_range = '';

                        if (!in_array("stop", $files_)) {

//                            $dir_ticker = '/home/talisant/tickers/' . $exchange . '/';
//                            $in_ticker_folder = scandir($dir_ticker . $pair);
//                            if (in_array("ticker.json", $in_ticker_folder)) {
//                                $str = file_get_contents($dir_ticker . $pair . '/ticker.json');
//                            }

                            $str = getTicker($exchange, $pair, 1);

                            $json = json_decode($str, true);

                            if($json == null){
                                $str = getTicker($exchange, $pair, 1);
                                $json = json_decode($str, true);
                            }

                            $ask = $json['ask'];
                            $bid = $json['bid'];

                            if ($bid > $data->upper_threshold || $ask < $data->lower_threshold) {
                                $out_of_range = "<span class='fa fa-power-off red-icon out-of' data-toggle='tooltip' data-placement='top' title='Out Of Range'></span> ";
//                                $out_of_range = "<span class='circle red out' data-toggle='tooltip' data-placement='top' title='Out Of Range'></span> ";
                            }
                        }
//      ================================================================================
//                        END OUT OF RANGE
//      ================================================================================



                        $range = $data->lower_threshold . '-' . $data->upper_threshold;

                        if ($data->earn == "Y") {
                            $part_1 = substr($pair, 0, 3);
                            $pair_arr = array(
                                'BCHABCUSD',
                                'BCHABCBTC',
                                'BCHABC-BTC',
                                'BCHABC-USDT',
                                'BCHABC-USD',
                            );


                            if(strpos($pair, '-')){
                                $part_1_ = explode('-',$pair);
                                $part_1 = $part_1_[0];
                            }
                            if (in_array($pair, $pair_arr)) {
                                    $part_1 = substr($pair, 3, 3);
                             }else if($pair == 'BCHSVUSD' || $pair == 'BCHSV-USDT' || $pair == 'BCHSVBTC'){
                                 $part_1 = substr($pair, 3, 2);
                             }


                            $earn = $part_1;

                        } else {
                            $part_2 = substr($pair, 3, 6);

                            if($pair == 'BCHABCUSD' || $pair == 'BCHABCBTC'){
                                $part_2 = substr($pair, 6, 9);
                            }else if($pair == 'BCHSVUSD' || $pair == 'BCHSVBTC'){
                                $part_2 = substr($pair, 5, 9);
                            }

                            if(strpos($pair, '-')){
                                $part_2_ = explode('-',$pair);
                                $part_2 = $part_2_[1];
                            }
                            $earn = $part_2;
                        }
                        if (in_array("stop", $files_)) {
                            $style = 'btn-danger';
                        }
                        if (in_array("remove", $files_)){
                            $style = "btn-warning";
                        }

                        ?>

                        <tr>

                            <td onclick="set.actionOpen('<?= $folder ?>')"><a
                                        href="javascript: void(0)"><? echo date("m-d H:i:s", ($folder + 4 * 3600));
                                    echo " (" . $folder . ")"; ?></a></td>
                            <td><?= $account; ?></td>
                            <td><?= $pair; ?></td>
                            <td>
<span class="coin_img" style="
    background-image: url(/img/coin/<?= $earn; ?>.png);
"></span>
                                <span> <?= $earn; ?></span>
                            </td>
                            <td><?= $total_investment; ?></td>
                            <td><?= $range; ?></td>
                            <td>
                                <div class="check_light">
                    <?php
                        if (!in_array("stop", $files_)) {
//      =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//                        CHECK LIGHT
//      =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

                            $status_color = 'red';
                            foreach ($comand_all_arr_tickers as $item) {
                                $pos_tick = strpos($item, $exchange . ' ' . $pair);
//                                if ($exchange == 'df') {
//                                    $pos_tick = strpos($item, $exchange);
//                                }
                                if ($pos_tick != false) {
                                    $status_color = 'green';
                                    break;
                                }
                            }
                            echo "<span class='fa fa-power-off $status_color-icon' data-toggle='tooltip' data-placement='top' title='Ticker'></span> ";


                            $status_color = 'red';
                            foreach ($comand_all_arr_edit as $item) {
                                $pos_edit = strpos($item, $folder);
                                if ($pos_edit != false) {
                                    $status_color = 'green';
                                    break;
                                }
                            }
                            echo "<span class='fa fa-power-off $status_color-icon' data-toggle='tooltip' data-placement='top' title='Edit'></span> ";


                            $status_color = 'red';
                            foreach ($comand_all_arr_order as $item) {
                                $pos_order = strpos($item, $folder);
                                if ($pos_order != false) {
                                    $status_color = 'green';
                                    break;
                                }
                            }
                            echo "<span class='fa fa-power-off $status_color-icon' data-toggle='tooltip' data-placement='top' title='Order'></span> ";

//      =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//                        END CHECK LIGHT
//      =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                        }
                    ?>
                                </div>
                            </td>
                            <td id="bot_<?= $folder ?>" style="text-align: right">

                                <?= $out_of_range; ?>
                                <button type="button" class="btn btn-info" data-toggle="modal"
                                        data-target="#myModalEdit"
                                        onclick="set.actionEdit(<?= $folder ?>)">Edit
                                </button>
                                <button type="button" class="hidden" style="display: none"
                                        onclick="set.actionOrder(<?= $folder ?>, this)">
                                    Order
                                </button>
                                <button type="button" class="btn <?= $pause_class; ?> "
                                        onclick="set.actionPause(<?= $folder ?>, this)">
<!--<span class="snow_img button_img" style="
    background-image: url(/img/1237.png);
    height: 75px;
    width: 75px;
    margin-left: -18px;
    margin-top: -17px;"
></span>-->
                                    <?= $pause_content; ?>
                                </button>
                                <?php
                                if($style != 'btn-danger' && $style != 'btn-warning'){
                                    ?>
                                    <button class="btn btn-info" onclick="set.stopButton_(<?= $folder ?>, this)">Stop</button>
                                <?php } ?>
                                <button type="button" class="btn <?= $style; ?>"
                                        onclick="set.actionRemove(<?= $folder ?>, this)">
<!--<span class="snow_img button_img" style="
    background-image: url(/img/1236.png);
    height: 75px;
    width: 94px;
    margin-left: -18px;
    margin-top: -27px;"
></span>-->
                                    Remove
                                </button>
                            </td>
                        </tr>

                        <?php

                     }
                }
            }

        }

        ?>
    </table>

    <!-- Modal -->

    <div class="modal fade" id="myModalOpen" role="dialog" style="text-align: left;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <!-- OPEN MODAL-->
            <div class="modal-content" style="display: table-row-group">
                <div class="modal-header" style="border: none">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="open_modal_header"></h4>
                    <div id="ticker_result"></div>
                    <div id="balance_result"></div>

                </div>

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#menu1">userSchedule</a></li>
                    <li><a data-toggle="tab" href="#menu2">orderHistory</a></li>
                    <li><a data-toggle="tab" href="#menu4">Balance</a></li>
                    <li><a data-toggle="tab" href="#menu3">newOrderInfo</a></li>
                </ul>

                <div class="tab-content">

                    <div id="menu1" class="tab-pane fade in active">
                        <div class="modal-body" style="padding-left: 30px;">
                            <table class="bots" id="schedule">
                                <div id="open_tbody_checkbox">
                                </div>
                                    <tr>
                                        <th>Level</th>
                                        <th>Amount</th>
                                        <th>Price</th>
                                        <th>Total_Value</th>
                                        <th>Distribution</th>
                                        <th>Cumulative_Distribution</th>
                                        <th>Profit</th>
                                        <th>Effective_Price</th>
                                        <th>RL</th>
                                        <th>Close line</th>
                                    </tr>
                                <tbody id="open_tbody">
                                </tbody>
                            </table>
                            <input value="Export as CSV Schedule" style="margin-top: 15px;" type="button" class="btn btn-info" onclick="$('#schedule').table2CSV({header:['Level','Amount','Price','Total_Value','Profit','Effective_Price','RL']}, 'schedule')">
                            <button class="btn btn-danger" id="close_position" onclick="set.lineClose(this)">close positions</button>

                        </div>
                    </div>


                    <div id="menu2" class="tab-pane fade">
                        <div class="modal-body" style="padding-left: 30px;">
                            <table class="bots">
                                <tbody id="open_tbody_history">
                                </tbody>
                            </table>
                            <a type="submit" class="btn btn-info" style="margin-top: 15px;"  onclick="set.downloadButton('orderHistory')">Export as CSV History</a>
                        </div>
                    </div>


                    <div id="menu3" class="tab-pane fade">
                        <div class="modal-body" style="padding-left: 30px;">
                            <div id="newOrderInfo_result"></div>
                        </div>
                    </div>


                    <div id="menu4" class="tab-pane fade">
                        <div class="modal-body" style="padding-left: 30px;">
                            <table class="bots" id="state_summary">
                                <tr>
                                    <th>Y</th>
                                    <th>X</th>
                                    <th>Left Investment</th>
                                    <th>Initial Investment</th>
                                    <th>Total Investment in Current Price</th>
                                    <th>Profit in Current Price</th>
                                    <th>Efficiency %</th>
                                </tr>
                                <tbody id="open_tbody_stateSummary">
                                </tbody>
                            </table>
                            <input value="Export as CSV Balance" style="margin-top: 15px;" type="button" class="btn btn-info" onclick="$('#state_summary').table2CSV({header:['Y', 'X', 'Left Investment', 'Initial Investment', 'Total Investment in Current Price', 'Profit in Current Price', 'Efficiency %']}, 'Balance')">
                        </div>
                    </div>


                </div>

                <input type="hidden" id="open_bot">
                <div class="modal-footer">
<!--                    <a type="submit" class="btn btn-info" style="float:left;"  onclick="set.downloadButton('userSchedule')">Download Schedule</a>-->
                    <button type="button" id="close_modal" class="btn btn-secondary" data-dismiss="modal">Cancel
                    </button>

                </div>
            </div>
            <!--END OPEN MODAL-->

        </div>
    </div>

    <div class="modal fade" id="myModal" role="dialog" style="text-align: left;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <!-- SETTINGS JSON-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!--                <form action="result.php" method="post">-->
                <form action="javascript: void(0)">

                    <div class="modal-body">


                        <div id="step_1">


                            <div class="form-group"> <!-- Full Name -->
                                <label for="name_id" class="control-label">ID</label>
                                <input type="text" class="form-control" id="name_id" required name="id" value="<?= time(); ?>"
                                       readonly>
                            </div>



                            <div class="form-group"> <!-- State Button -->
                                <label for="account" class="control-label">Exchange</label>
                                <select class="form-control" id="exchange" required name="Exchange" onchange="set.GetExchange()">
                                    <option value="">select...</option>

                                    <option value="binance">Binance</option>
                                    <option value="bitfinex">Bitfinex</option>
                                    <option value="df">DF</option>

                                </select>
                            </div>

                            <div class="form-group"> <!-- State Button -->
                                <label for="account" class="control-label">Account</label>
                                <select class="form-control" id="account" required name="Account">
                                    <option value="">select...</option>

                                </select>
                            </div>

                            <div class="form-group"> <!-- State Button -->
                                <label for="pair" class="control-label">Pair</label>
                                <select class="form-control" id="pair" required name="pair" onchange="set.ChangePair()">
                                    <option value="">select...</option>
                                </select>
                            </div>

                            <div class="form-group"> <!-- State Button -->
                                <label for="earn" class="control-label">Earn</label>
                                <select class="form-control" id="earn" required name="earn">
                                    <!--                                <option value="">select...</option>-->
                                    <option value="Y" id="Y">Y</option>
                                    <option value="X" id="X">X</option>
                                </select>
                            </div>


                            <hr>
                            <div class="form-group" style="text-align: right">
                                <a class="btn btn-info" id="next">Next</a>
                            </div>

                        </div>

                        <div id="step_2">

                            <h3>Range</h3>

                            <div class="form-group">
                                <label for="add_asset" class="control-label">Investment</label>
                                <input type="text" class="form-control" id="investment" required name="investment"
                                       placeholder="investment">
                            </div>
                            <div class="form-group">
                                <label for="upper_threshold" class="control-label">Upper Threshold</label>
                                <input type="text" class="form-control" id="upper_threshold"
                                       required name="upper_threshold">
                            </div>

                            <div class="form-group">
                                <label for="lower_threshold" class="control-label">Lower Threshold</label>
                                <input type="text" class="form-control" id="lower_threshold"
                                       required name="lower_threshold">
                            </div>

                            <div class="form-group">
                                <label for="levels" class="control-label">Levels</label>
                                <input type="number" class="form-control" id="levels" required name="levels">
                            </div>

                            <hr>
                            <h3>Lots</h3>


                            <div class="form-group">
                                <label for="x_min_lot" class="control-label">X minimal Lot</label>
                                <input type="text" class="form-control" id="x_min_lot" required name="x_min_lot">
                            </div>

                            <div class="form-group">
                                <label for="y_min_lot" class="control-label">Y minimal Lot</label>
                                <input type="text" class="form-control" id="y_min_lot" required name="y_min_lot">
                            </div>


                            <hr>
                            <h3>Costs</h3>


                            <div class="form-group">
                                <label for="fee" class="control-label">Fee %</label>
                                <input type="text" class="form-control" id="fee" required name="fee"
                                       placeholder="%">
                            </div>

                            <div class="form-group">
                                <label for="spread" class="control-label">Spread %</label>
                                <input type="text" class="form-control" id="spread" required name="spread"
                                       placeholder="%">
                            </div>

                            <hr>
                            <h3>Coefficient</h3>


                            <div class="form-group">
                                <label for="coef_lambda" class="control-label">λ</label>
                                <input type="text" class="form-control" id="coef_lambda"
                                       required name="coef_lambda">
                            </div>

                            <div class="form-group">
                                <label for="coef_RR" class="control-label">RR</label>
                                <input type="text" class="form-control" id="coef_RR" required name="coef_RR">
                            </div>


                        </div>
                    </div>

                    <div class="modal-footer" id="modal_footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-info" id="send">Send</button>
                    </div>

                </form>

            </div>
            <!--END SETTINGS JSON-->

        </div>
    </div>


    <div class="modal fade" id="myModalEdit" role="dialog" style="text-align: left;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <!-- EDIT SETTINGS-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit</h4>
                </div>
                <!--                <form action="edit.php" method="post">-->
                <form action="javascript: void(0)" method="post" id="settingForm">
                    <div class="modal-body">


                        <div class="form-group"> <!-- Full Name -->
                            <label for="name_id" class="control-label">ID</label>
                            <input type="text" class="form-control" id="name_id_edit" required name="id" value="<?=time();?>" readonly>
                        </div>

                        <div class="form-group"> <!-- Full Name -->
                            <label for="exchange" class="control-label">Exchange</label>
                            <input type="text" class="form-control" id="exchange_edit" required name="exchange" value="" readonly>
                        </div>


                        <div class="form-group"> <!-- Full Name -->
                            <label for="name_id" class="control-label">Account</label>
                            <input type="text" class="form-control" id="account_edit" required name="account_edit" value="" readonly>
                        </div>

                        <div class="form-group"> <!-- State Button -->
                            <label for="pair" class="control-label">Pair</label>
                            <input type="text" class="form-control" id="pair_edit" required name="pair" value="" readonly>
                        </div>

                        <div class="form-group"> <!-- State Button -->
                            <label for="earn" class="control-label">Earn</label>
                            <input type="text" class="form-control" id="earn_edit" required name="earn" value="" readonly>
                        </div>

                        <hr>


                        <h3>Range</h3>


                        <div class="form-group">
                            <label for="add_asset" class="control-label">Investment</label>
                            <input type="text" class="form-control" id="investment_edit" required name="investment" readonly>
                        </div>
                        <div class="form-group" id="add_asset_for">
                            <label for="add_asset" class="control-label">Add asset</label>
                            <input type="text" class="form-control" id="add_asset_edit" name="add_asset" placeholder="add asset">
                        </div>

                        <div class="form-group">
                            <label for="upper_threshold" class="control-label">Upper Threshold</label>
                            <input type="text" class="form-control" id="upper_threshold_edit" required name="upper_threshold">
                        </div>

                        <div class="form-group">
                            <label for="lower_threshold" class="control-label">Lower Threshold</label>
                            <input type="text" class="form-control" id="lower_threshold_edit" required name="lower_threshold">
                        </div>

                        <div class="form-group">
                            <label for="levels" class="control-label">Levels</label>
                            <input type="number" class="form-control" id="levels_edit" required name="levels">
                        </div>

                        <hr>
                        <h3>Lots</h3>


                        <div class="form-group">
                            <label for="x_min_lot" class="control-label">X minimal Lot</label>
                            <input type="text" class="form-control" id="x_min_lot_edit" required name="x_min_lot">
                        </div>

                        <div class="form-group">
                            <label for="y_min_lot" class="control-label">Y minimal Lot</label>
                            <input type="text" class="form-control" id="y_min_lot_edit" required name="y_min_lot">
                        </div>


                        <hr>
                        <h3>Costs</h3>


                        <div class="form-group">
                            <label for="fee" class="control-label">Fee %</label>
                            <input type="text" class="form-control" id="fee_edit" required name="fee" placeholder="%">
                        </div>

                        <div class="form-group">
                            <label for="spread" class="control-label">Spread %</label>
                            <input type="text" class="form-control" id="spread_edit" required name="spread" placeholder="%">
                        </div>

                        <hr>
                        <h3>Coefficient</h3>


                        <div class="form-group">
                            <label for="coef_lambda" class="control-label">λ</label>
                            <input type="text" class="form-control" id="coef_lambda_edit" required name="coef_lambda">
                        </div>

                        <div class="form-group">
                            <label for="coef_RR" class="control-label">RR</label>
                            <input type="text" class="form-control" id="coef_RR_edit" required name="coef_RR">
                        </div>

                    </div>

                    <div class="modal-footer" id="hide_for_copy">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <a type="submit" class="btn btn-info"  onclick="set.copyButton('<?=$user_id;?>')" title='Create New Bot With Same Parameters'>Create Copy</a>
                        <a type="submit" class="btn btn-info"  onclick="set.stopButton(this)">Stop</a>
                        <button type="submit" class="btn btn-success" onclick="set.saveButton()">Save</button>
                    </div>

                </form>
            </div>
            <!-- END EDIT SETTINGS-->
        </div>
    </div>

</div>

<?php include './footer.php'; ?>
