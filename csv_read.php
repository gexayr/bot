<?php

//ini_set("display_errors", 1);
//error_reporting(E_ALL);

if (isset($_POST['id'])) {
    $id = $_POST['id'];
    $file_name = $_POST['file'];
    $file_ = "$file_name.csv";


    $dir = $_SERVER['DOCUMENT_ROOT'] . "/bot/$id";
    $array_in = scandir("$dir");

    if ($file_name == 'newOrderInfo') {
        $content = file_get_contents("bot/$id/newOrderInfo");
        if($content == false){
            die("$file_name not found");
        }
        echo $content;
        die;
    }
    if (!in_array($file_, $array_in)) {
        echo "$file_ not found";
        die;
    }

    function createTable($id, $file_name, $count_func = 1)
    {

        $file = "bot/$id/$file_name.csv";
        $csv = array_map('str_getcsv', file($file));

        $json = file_get_contents("bot/$id/$id.json");
        $data = (json_decode($json));

        $content = '';

        if (!empty($csv)) {
            $exchange = $data->exchange;
            $earn = $data->earn;
            $investment = $data->investment;
            $pair = $data->pair;
            $word = '';
            if ($exchange == 'binance') {
                $word = 'time';
            } elseif ($exchange == 'bitfinex') {
                $word = 'timestamp';
            } elseif ($exchange == 'df') {
                $word = 'ReceiveTime';
            }
            $numb = 999;
            if ($file_name == 'userSchedule') {

                //_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
                //+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+

                $coef = (1 + $data->coef_RR / 100) / (pow((1 - $data->fee / 100), 2));
                //_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
                //+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+

                $RL = ($data->upper_threshold - $data->lower_threshold) / $data->levels;
                $number = $data->upper_threshold;
                $eff_price = 0.0;
                foreach ($csv as $k => $item) {
                    $content .= '<tr>';
                    foreach ($item as $key => $value) {

                        if ($value != '') {
                            $content .= "<td> $value</td>";
                            if ($key == 2) {
                                if ($earn == 'X') {
                                    $eff_price = $value * $coef;
                                } else {
                                    $eff_price = $value / $coef;
                                }
                            }
                        }
                    }
                    $content .= "<td>$eff_price</td>";

                    if ($number == $data->upper_threshold) {
                        $number = $data->upper_threshold . '<';
                    }
                    $content .= "<td>$number</td>";
                    $number = $number - $RL;
                    $checkbox = "<input type='checkbox' class='form-check-input' data-id=".(count($csv) - ($k+1)).">";
                    $content .= "<td>$checkbox</td>";

                    $content .= '</tr>';
                }
            } elseif ($file_name == 'orderHistory') {
                foreach ($csv as $item) {
                    $content .= '<tr>';
                    foreach ($item as $key => $value) {

                        if ($key == $numb) {
                            if ($exchange == 'binance' || $exchange == 'df') {
                                $value = mb_substr($value, 0, -3);
                            }
                            $value = date('m-d H:i:s', $value + (4 * 3600));
                        }
                        if ($value == $word) {
                            $numb = $key;
                        }
                        //                    if ($value != '') {
                        $content .= "<td> $value</td>";
                        //                    }
                    }
                    $content .= '</tr>';

                }
            } elseif ($file_name == 'stateSummary') {
                function getTicker($exchange, $pair, $count)
                {
                    $dir_ticker = '/home/talisant/tickers/' . $exchange . '/';
                    $in_ticker_folder = scandir($dir_ticker . $pair);
                    if (in_array("ticker.json", $in_ticker_folder)) {
                        $str = file_get_contents($dir_ticker . $pair . '/ticker.json');
                    }
                    $json = json_decode($str, true);

                    if ($json == null & $count < 1000) {
                        $count++;
                        getTicker($exchange, $pair, $count);
                    }
                    return $json;
                }

                $json = getTicker($exchange, $pair, 1);
                $ask = $json['ask'];
                $bid = $json['bid'];

                foreach ($csv as $item) {
                    $content .= '<tr>';
                    foreach ($item as $key => $value) {
                        if ($value != '') {
                            if ($earn == 'Y' && $key == 0) $value = -$value;
                            if ($earn == 'X' && $key == 1) $value = -$value;
                            $content .= "<td> $value</td>";
                        }

                        if ($key == 2) {
                            $left_invest = $value;
                        } elseif ($key == 1) {
                            $result_x = $value;
                        } elseif ($key == 0) {
                            $result_y = $value;
                        }
                    }

                    if ($earn == 'Y') {
                        $total_in = $left_invest + ($result_x / $ask);
                    } elseif ($earn == 'X') {
                        $total_in = $left_invest + ($result_y * $bid);
                    }

                    $profit = $total_in - $investment;
                    $efficiency = ($profit / $investment) * 100;

                    $content .= "<td> $investment</td>";
                    $content .= "<td> $total_in</td>";
                    $content .= "<td> $profit</td>";
                    $content .= "<td> $efficiency</td>";
                    $content .= '</tr>';
                }
            }
        } elseif ($count_func < 1000 && empty($csv)) {
            $count_func++;
            createTable($id, $file_name, $count_func);

        }
        return $content;
    }
    $content = createTable($id, $file_name, 1);
    echo $content;
}