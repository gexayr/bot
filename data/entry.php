<?php
require "../pdo/config.php";

$bot_data = json_encode($_POST);

    try  {
        $connection = new PDO($dsn, $username, $password, $options);

        $new_user = array(
            "user_id" => $_POST['user'],
            "account"  => $_POST['account'],
            "bot_id"  => $_POST['id'],
            "bot_data"     => $bot_data,
        );

        $sql = sprintf(
            "INSERT INTO %s (%s) values (%s)",
            "bot",
            implode(", ", array_keys($new_user)),
            ":" . implode(", :", array_keys($new_user))
        );

        $statement = $connection->prepare($sql);
        $statement->execute($new_user);
    } catch(PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
        die;
    }

$order_history = '';
$user_schedule = '';
$state_summary = '';

try  {
    $connection = new PDO($dsn, $username, $password, $options);

    $new_user = array(
        "bot_id"            => $_POST['id'],
        "user_id"          => $_POST['user'],
        "bot_data"          => $bot_data,
        "order_history"     => $order_history,
        "user_schedule"     => $user_schedule,
        "state_summary"     => $state_summary,
    );

    $sql = sprintf(
        "INSERT INTO %s (%s) values (%s)",
        "edit",
        implode(", ", array_keys($new_user)),
        ":" . implode(", :", array_keys($new_user))
    );

    $statement = $connection->prepare($sql);
    $statement->execute($new_user);
} catch(PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
    die;
}
