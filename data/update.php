<?php
require "../pdo/config.php";
ini_set("display_errors",1);
error_reporting(E_ALL);
$bot_data = json_encode($_POST);
$id = $_POST['id'];
$bot_type = '';
if(isset($_POST['bot_type'])){
    $bot_type = $_POST['bot_type'];
}

$order_history = file_get_contents("../bot/$id/orderHistory.csv");
$user_schedule = file_get_contents("../bot/$id/userSchedule.csv");
$state_summary = file_get_contents("../bot/$id/stateSummary.csv");
function getBalance($count, $bot_type, $id){
    $user_schedule = '';
    $count++;
    if($count < 1000){
        $user_schedule = file_get_contents("/var/www/html/$bot_type/bot/$id/Balance.csv");
        if( $user_schedule == null){
            getBalance($count, $bot_type, $id);
        }else{
            return $user_schedule;
        }
    }
}
if(strlen($_POST['earn']) > 2){
    $user_schedule =  getBalance(1, $bot_type, $id);
}
var_dump($user_schedule);

try  {
        $connection = new PDO($dsn, $username, $password, $options);

        $new_user = array(
            "bot_id"            => $_POST['id'],
            "user_id"          => $_POST['user'],
            "bot_data"          => $bot_data,
            "order_history"     => $order_history,
            "user_schedule"     => $user_schedule,
            "state_summary"     => $state_summary,
        );

        $sql = sprintf(
            "INSERT INTO %s (%s) values (%s)",
            "edit",
            implode(", ", array_keys($new_user)),
            ":" . implode(", :", array_keys($new_user))
        );

        $statement = $connection->prepare($sql);
        $statement->execute($new_user);
    } catch(PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
        die;
    }

    try {
        $connection = new PDO($dsn, $username, $password, $options);

        $user =[
            "bot_id"        => $id,
            "bot_data"      => $bot_data
        ];

        $sql = "UPDATE bot
            SET bot_data = :bot_data
            WHERE bot_id = :bot_id";

        $statement = $connection->prepare($sql);
        $statement->execute($user);
    } catch(PDOException $error) {
        echo $error->getMessage();
    }

    echo 'success_update';

