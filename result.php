<?php


// +++++++++++++++++++++===
if(isset($_POST['id'])) {
    $_POST['investment'] = (float)(filter_var( $_POST['investment'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['upper_threshold'] = (float)(filter_var( $_POST['upper_threshold'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['lower_threshold'] = (float)(filter_var( $_POST['lower_threshold'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['levels'] = (float)(filter_var( $_POST['levels'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['x_min_lot'] = (float)(filter_var( $_POST['x_min_lot'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['y_min_lot'] = (float)(filter_var( $_POST['y_min_lot'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['fee'] = (float)(filter_var( $_POST['fee'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['spread'] = (float)(filter_var( $_POST['spread'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['coef_lambda'] = (float)(filter_var( $_POST['coef_lambda'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['coef_RR'] = (float)(filter_var( $_POST['coef_RR'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));

    if (!file_exists('bot/' . $_POST['id'])) {
        mkdir('bot/' . $_POST['id'], 0777, true);
    }

    $arr = $_POST;
    $str = "{\n";
    foreach ($arr as $key => $val) {
        $str .= '"' . $key . '"' . ':' . '"' . $val . '"' . ",\n";
    }

    $str = mb_substr($str, 0, -2);

    file_put_contents('bot/' . $_POST['id'] . "/" . $_POST['id'] . '.json', $str . "\n}");


    $settings['main_asset_min_lot'] = $arr['x_min_lot'];
    $settings['tool_asset_min_lot'] = $arr['y_min_lot'];
    $settings['RR'] = $arr['coef_RR'];
    $settings['lambda_'] = $arr['coef_lambda'];
    $settings['min_'] = $arr['lower_threshold'];
    $settings['max_'] = $arr['upper_threshold'];
    $settings['fee'] = $arr['fee'];
    $settings['spread'] = $arr['spread'];
    $settings['levels_amount'] = $arr['levels'];
    $settings['asset'] = $arr['investment'];
    $str_set = "{\n";
    foreach ($settings as $key => $val) {
        $str_set .= '"' . $key . '"' . ':' . $val . ",\n";
    }

    $str_set = mb_substr($str_set, 0, -2);


    file_put_contents('bot/' . $_POST['id'] . "/settings.json", $str_set . "\n}");


    file_put_contents('bot/' . $_POST['id'] . "/" . 'edit', date("Y-m-d H-i-s")  . "\n", FILE_APPEND);

    unlink('bot/' . $_POST['id'] . "/" . 'pause');
}
//header('Location: /');
$dir = ($_SERVER['DOCUMENT_ROOT']);
$bot_dir = $dir."/bot/".$_POST['id']."/";
//$key_dir = $dir."/base/".$_POST['account']."/key.json";
$key_dir = $dir."/base/" . $_POST["exchange"] . '/' . $_POST['account'] . "/key.json";

$pair = $_POST["pair"] ;

$sex = $_POST["earn"];
$time = time();
$command1 = null;
if($_POST['exchange'] == 'binance'){
    $command1 = escapeshellcmd("/usr/bin/python  /home/talisant/AlgoXY_new/binance_order.py ${bot_dir} ${key_dir} >& ${bot_dir}order.log  & ");
}elseif($_POST['exchange'] == 'bitfinex'){
    $command1 = escapeshellcmd("/usr/bin/python  /home/talisant/AlgoXY_new/order_new_test.py ${bot_dir} ${key_dir} >& ${bot_dir}order.log  & ");
}elseif($_POST['exchange'] == 'df'){
    $command1 = escapeshellcmd("/usr/bin/python  /home/talisant/AlgoXY_new/df_order.py ${bot_dir} ${key_dir} >& ${bot_dir}order.log  & ");
}
exec($command1);