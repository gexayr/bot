<?php


if(isset($_POST['id'])) {
    $_POST['investment'] = (float)(filter_var( $_POST['investment'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['add_asset'] = (float)(filter_var( $_POST['add_asset'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['upper_threshold'] = (float)(filter_var( $_POST['upper_threshold'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['lower_threshold'] = (float)(filter_var( $_POST['lower_threshold'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['levels'] = (float)(filter_var( $_POST['levels'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['x_min_lot'] = (float)(filter_var( $_POST['x_min_lot'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['y_min_lot'] = (float)(filter_var( $_POST['y_min_lot'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['fee'] = (float)(filter_var( $_POST['fee'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['spread'] = (float)(filter_var( $_POST['spread'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['coef_lambda'] = (float)(filter_var( $_POST['coef_lambda'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));
    $_POST['coef_RR'] = (float)(filter_var( $_POST['coef_RR'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ));

    if (!file_exists('bot/' . $_POST['id'])) {
        mkdir('bot/' . $_POST['id'], 0777, true);
    }
    $_POST['investment'] = $_POST['investment'] + $_POST['add_asset'];
    $add_asset = $_POST['add_asset'];
    unset($_POST['add_asset']);
    $arr = $_POST;
    $str = "{\n";

    foreach ($arr as $key => $val) {
        $str .= '"' . $key . '"' . ':' . '"' . $val . '"' . ",\n";
    }

    $str = substr($str, 0, -2);

    file_put_contents('bot/' . $_POST['id'] . "/" . $_POST['id'] . '.json', $str . "\n}" . "\n");


    $settings['main_asset_min_lot'] = $arr['x_min_lot'];
    $settings['tool_asset_min_lot'] = $arr['y_min_lot'];
    $settings['RR'] = $arr['coef_RR'];
    $settings['lambda_'] = $arr['coef_lambda'];
    $settings['min_'] = $arr['lower_threshold'];
    $settings['max_'] = $arr['upper_threshold'];
    $settings['fee'] = $arr['fee'];
    $settings['spread'] = $arr['spread'];
    $settings['levels_amount'] = $arr['levels'];
    $settings['asset'] = $add_asset;
    $str_edit = "{\n";
    foreach ($settings as $key => $val) {
        $str_edit .= '"' . $key . '"' . ':' . $val . ",\n";
    }

    $str_edit = mb_substr($str_edit, 0, -2);


    file_put_contents('bot/' . $_POST['id'] . '/settings.json', $str_edit . "\n}" . "\n");
    file_put_contents('bot/' . $_POST['id'] . "/" . 'edit', date("Y-m-d H-i-s")  . "\n");

}

echo('success');