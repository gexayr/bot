<?php require '../header.php'; ?>
<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL);

require "../pdo/config.php";
require "../pdo/common.php";



if (isset($_POST['submit'])) {
    if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

    try  {
        $connection = new PDO($dsn, $username, $password, $options);

        $new_user = array(
            "login" => $_POST['login'],
            "password"  => md5($_POST['password']),
            "role"     => 'user',
        );

        $sql = sprintf(
            "INSERT INTO %s (%s) values (%s)",
            "users",
            implode(", ", array_keys($new_user)),
            ":" . implode(", :", array_keys($new_user))
        );

        $statement = $connection->prepare($sql);
        $statement->execute($new_user);
    } catch(PDOException $error) {
//        echo $sql . "<br>" . $error->getMessage();
        echo '     <div class="alert alert-danger alert-dismissible fade in col-sm-6 col-sm-offset-3" style="text-align: center">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong> '.$error->getMessage().'
        </div>';
        die;
    }
}
?>

<?php if (isset($_POST['submit']) && $statement) : ?>
    <div class="alert alert-success alert-dismissible fade in col-sm-6 col-sm-offset-3" style="text-align: center; margin-bottom: 0">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> User with login  <strong> <?php echo escape($_POST['login']); ?></strong> successfully added.
    </div>
    <script>
        setTimeout(function(){
            $( ".alert-success" ).fadeOut();
            window.location.href = '/login'

        }, 2000);
    </script>

<?php endif; ?>



    <div class="container">
        <h2 class="text-center" style="margin-top: 35px">Registration</h2>
        <div class="card card-container col">
            <p id="profile-name" class="profile-name-card"></p>
            <form class="form-signin col-sm-4 col-sm-offset-4" method="post">
                <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">

                <span id="reauth-email" class="reauth-email"></span>
                <div class="form-group">
                    <input type="text" id="inputEmail" class="form-control" placeholder="Login" required autofocus name="login">
                </div name="">
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" required name="password">
                <div id="remember" class="checkbox">

                </div>
                <input class="btn btn-info col-sm-4 col-sm-offset-4" type="submit" name="submit" value="Sign in">
            </form><!-- /form -->

        </div><!-- /card-container -->
    </div><!-- /container -->


<?php require '../footer.php'; ?>