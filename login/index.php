<?php
ob_start();
?>
<?php require '../header.php'; ?>
<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

require "../pdo/config.php";
require "../pdo/common.php";



if (isset($_POST['submit'])) {
//    if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();
    try  {
        $connection = new PDO($dsn, $username, $password, $options);

        $sql = "SELECT * 
            FROM users
            WHERE login = :login 
            AND password = :password";

        $login = $_POST['login'];
        $password = md5($_POST['password']);
        $statement = $connection->prepare($sql);
        $statement->bindParam(':login', $login, PDO::PARAM_STR);
        $statement->bindParam(':password', $password, PDO::PARAM_STR);
        $statement->execute();

        $result = $statement->fetchAll();
    } catch(PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
//$login = 'admin';
//header('Location: /');
    }
}
?>

<?php
if (isset($_POST['submit'])) {
    if ($result && $statement->rowCount() > 0) {
        $cookie_name = "login";
        $cookie_value = $_POST['login'];
        setcookie($cookie_name, $cookie_value, time() + (3600 * 18), "/"); // 86400 = 1 day
        $cookie_name = "key";
        $cookie_value = md5($_POST['login']);
        setcookie($cookie_name, $cookie_value, time() + (3600 * 18), "/"); // 86400 = 1 day
        sleep(1);
        if($_POST['login'] == 'admin'){
            header('Location: /admin/');
        }else{
            header('Location: /');
        }

//        echo "WRITE COOKIES";

    } else { ?>
        <div class="alert alert-danger alert-dismissible fade in col-sm-6 col-sm-offset-3" style="text-align: center; margin-bottom: 0">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong> Login or password not found.
        </div>
    <?php }
} ?>



    <div class="container">
            <h2 class="text-center" style="margin-top: 35px">Login</h2>
        <div class="card card-container col">
            <p id="profile-name" class="profile-name-card"></p>
            <form class="form-signin col-sm-4 col-sm-offset-4" method="post">
                <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">

                <span id="reauth-email" class="reauth-email"></span>
                <div class="form-group">
                    <input type="text" id="inputEmail" class="form-control" placeholder="Login" required autofocus name="login">
                </div name="">
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" required name="password">
                <div id="remember" class="checkbox">

                </div>
                <input class="btn btn-info col-sm-4 col-sm-offset-4" type="submit" name="submit" value="Sign in">
            </form><!-- /form -->

        </div><!-- /card-container -->
    </div><!-- /container -->


<?php require '../footer.php'; ?>
