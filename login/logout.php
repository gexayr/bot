<?php

if (isset($_COOKIE['login'])) {
    unset($_COOKIE['key']);
    unset($_COOKIE['login']);
    setcookie('key', null, -1, '/');
    setcookie('login', null, -1, '/');
    header('Location: /');
} else {
    header('Location: /');
}
